﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System.Threading.Tasks;
using UnityEngine.UI;

public class GooglePlayClient : MonoBehaviour
{
    public Text text;

    // Start is called before the first frame update
#if UNITY_ANDROID
    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .RequestServerAuthCode(false /* Don't force refresh */)
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        //SignIn();
    }

#endif

    public void SignIn()
    {
#if UNITY_ANDROID
       Social.localUser.Authenticate((bool success) => {
           SignInCallback(success);
        });

      /*
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
        }
        else
        {
            // Sign out of play games
            PlayGamesPlatform.Instance.SignOut();
        }*/
#endif
    }

    public void SignInCallback(bool success)
    {
     

    }
}
