﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public Color unlockedLevelColor;
    public Color lockedLevelColor;

    public LaserColors laserColors;

    public Mode currentMode;
    public LayerMask gamePiecesMask;
    public static GameMaster Master;
    private Camera mainCam;
   
    public Utility utilty;
    public LayerMask cellsLayerMask;

    public LaserManager laserManager;
    public Level currentLevel;
    public InventoryManager inventory;
    public LevelBuilderScript levelBuilder;

    public EndSreenScript endSreen;

    public ExistingPieces existingPieces;

    public Menu menu;
    // Start is called before the first frame update
    void Start()
    {
       if(Master == null)
        {
            Master = this;
        }
        if (!PlayerPrefs.HasKey("levelUnlocked"))
        {
            PlayerPrefs.SetInt("levelUnlocked", 1);
        }
    }


    public Camera MainCamera()
    {
        if (mainCam == null)
        {
            mainCam = Camera.main;
        }
        return mainCam;
    }

    /// <summary>
    /// Exits play and opens the required menu panel
    /// </summary>
    /// <param name="i"> the index of the requested panel </param>
    public void ExitPlay(int i)
    {
        inventory.HideAll();
        levelBuilder.grid.Clear();
        laserManager.Reset();
        StartCoroutine(menu.Display(i));
    }

    public void SetMode(Mode m)
    {
        currentMode = m;

    }
    public Mode GetMode()
        {
            return currentMode;
        }

}


public enum Mode { play,build}