﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine;
using SimpleJSON;
using GooglePlayGames;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;

public class GameWriter : MonoBehaviour {
    public HexGrid hg;
    public DatabaseReference database;

    public Inventory givenInventory;


    public void FetchAndPlayRandomLevelOnDB()
    {
        StartCoroutine(RandomLevelFromDatabase());
    }

    public IEnumerator RandomLevelFromDatabase()
        {
       
        //int randomIndex = UnityEngine.Random.Range(0, numberOfUsers);

        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://mirrorshex.firebaseio.com/");
        // Get the root reference location of the database.
        database = FirebaseDatabase.DefaultInstance.RootReference;
        
        database = database.Child("levels");
        
        Debug.Log("Random:");
        
       
        bool failedTask = false;
     
       
            string levelJson = null;
          
            
            database.GetValueAsync().ContinueWith(t => {
                if (t.IsFaulted)
                {
                    Debug.Log("Failed to retrieve a random level  " );
                    failedTask = true;
                }
                else
                {

                    Debug.Log("Random level retrieved: ");
                    DataSnapshot snap = t.Result;
                  
                    Dictionary<string, object> val = (Dictionary<string, object>)snap.Value;
                    string levelID = "";
                    List<string> ids = new List<string>();
                    int randomIndex = UnityEngine.Random.Range(0, val.Count);
                    foreach (KeyValuePair<string, object> levelKeyVal in val)
                    {
                        ids.Add(levelKeyVal.Key);


                    }
                    levelID = ids[randomIndex];
                    levelJson = snap.Child(levelID).GetRawJsonValue();
                    database = database.Child(levelID);
                    Debug.Log(levelID);
                    Debug.Log(levelJson);
                }
                return;
            });

            yield return new WaitUntil(() => levelJson != null || failedTask);
                    GameMaster.Master.menu.PlayLevel( levelJson);


    }


    public void writeHexGrid(){

        string levelString = getCurrentLevelJson();
        Debug.Log(levelString);
        GameMaster.Master.levelBuilder.BeginValidatingLevel(levelString);
       // PublishLevel(levelString);
	}

    public void PublishCurrentLevel()
    {
        PublishLevel(GameMaster.Master.currentLevel.getJson());
    }
    private void PublishLevel(string json)
    {
        
        string userID = GetUserId();
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://mirrorshex.firebaseio.com/");
        // Get the root reference location of the database.
        database = FirebaseDatabase.DefaultInstance.RootReference;
        //database = database.Child("users");
        database = database.Child("levels");

        //database = database.Child(userID);
        database = database.Push();
        if (database != null)
        {
            database.SetRawJsonValueAsync(json);
        }

    }

    public string GetUserId()
    {
        string authCode = PlayGamesPlatform.Instance.GetServerAuthCode();
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        Firebase.Auth.Credential credential = Firebase.Auth.PlayGamesAuthProvider.GetCredential(authCode);
        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });

        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        string userID = "";
        if (user != null)
        {
            string playerName = user.DisplayName;
            // The user's Id, unique to the Firebase project.
            // Do NOT use this value to authenticate with your backend server, if you
            // have one; use User.TokenAsync() instead.
            userID = user.UserId;
        }
        return userID;
    }

    public string getCurrentLevelJson()
    {
        var hgWriter = new JSONObject();
        hgWriter.Add("height", hg.height);
        hgWriter.Add("width", hg.width);
        hgWriter.Add("authorId", GetUserId());
        JSONArray cellsArray = new JSONArray();
        int rowCounter = 0;
        foreach (HexCell[] hexRow in hg.cellsArray)
        {
            JSONArray cellsRow = new JSONArray();
            cellsArray[rowCounter] = cellsRow;
            int hexCounter = 0;
            if (hexRow != null)
            {
                foreach (HexCell hex in hexRow)
                {
                    if (hex != null)
                    {
                        if (!hex.IsActive())
                        {
                            cellsRow[hexCounter].Add("inactive", true);
                        }
                        else
                        {
                            if (hex.GetPiece() != null)
                            {
                                GamePiece piece = hex.piece;
                                //cellsRow[hexCounter].Add("empty", false);
                                cellsRow[hexCounter].Add("pieceID", piece.ID);
                                cellsRow[hexCounter].Add("orientation", piece.orientation);
                                if (piece.isColorable())
                                {
                                    cellsRow[hexCounter].Add("color", (int)((Colorable)piece).GetLaserColor());
                                }
                            }
                            else
                            {
                                cellsRow[hexCounter].Add("empty", true);
                            }
                        }
                        
                        hexCounter++;
                    }
                }
            }
            rowCounter++;
        }
        hgWriter.Add("cells", cellsArray);
        JSONArray inventory = new JSONArray();
        foreach (int id in givenInventory.GetIdList())
        {
            inventory.Add(id);
        }
        hgWriter.Add("inventory", inventory);
        return hgWriter.ToString();
    }
	public void writeUser(string userID, string name){
		// Set up the Editor before calling into the realtime database.
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://mirrorshex.firebaseio.com/");
		// Get the root reference location of the database.
		database = FirebaseDatabase.DefaultInstance.RootReference;
		database = database.Child("users");

		var hgWriter = new JSONObject();
		hgWriter.Add("userID", userID);
		hgWriter.Add("name", name);
	}

	public void setHexGrid(HexGrid hg){
		this.hg = hg;
	}
}
