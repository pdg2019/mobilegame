﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExistingPieces : MonoBehaviour
{
    public List<GamePiece> pieces;
    public Dictionary<int, GamePiece> idToPrefabTable;

    public void Start()
    {
        FillDictionnary();
    }
    public void FillDictionnary()
    {
        idToPrefabTable = new Dictionary<int, GamePiece>();
        foreach(GamePiece gp in pieces)
        {
            idToPrefabTable.Add(gp.ID, gp);
        }
    }
    public GamePiece GetPiece(int id)
    {
        GamePiece piece;
        if (idToPrefabTable.TryGetValue(id, out piece))
        {
            return piece;
        }
        else
        {
            return null;
        };
        
    }
    public List<GamePiece> AllAvailablePieces()
    {
        return new List<GamePiece>(idToPrefabTable.Values);
    }
}
