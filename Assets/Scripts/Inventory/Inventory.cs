﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    private Animator animator;
    
    private GameMaster gameMaster;
    private bool pressingDown;
    private float lastPressTime;
    

  

    public Button nextButton;
    public Button prevButton;
    public int currentIndex;
    private InventorySlot selectedSlot;
   

    public List<int> idsInDisplayOrder;
    public Dictionary<int, int> idAmountDictionnary;

    public List<InventorySlot> slots;

    // Start is called before the first frame update
    void Start()
    {
        gameMaster = GameMaster.Master;
     

#if UNITY_ANDROID
        Input.multiTouchEnabled = false;

#endif
    }

    // Update is called once per frame
    void Update()
    {

    }
  
    
    public void Display()
    {
        Display(currentIndex);
    }
    private void Display(int index)
    {
      
           index=Mathf.Clamp(index,0, Mathf.Max(0,idsInDisplayOrder.Count-5));
       // Debug.Log("displayng:" + index);
            
            for(int i= 0; i < 5; i++)
            {
               
                if (index+i< idsInDisplayOrder.Count)
                {
                    int pieceIndex = idsInDisplayOrder[index + i];
                    int amount = -1;
                    if(idAmountDictionnary.TryGetValue(pieceIndex,out amount))
                    {
                //   Debug.Log(slots[i]);
                    
                        slots[i].SetPiece(GameMaster.Master.existingPieces.GetPiece(pieceIndex),amount);

                    }
                    
                }
                else
                {
                    slots[i].SetPiece(null,0);
                }
            }
            currentIndex = index;
            CheckIfDisplayingLimits();
        
    }
   
    

    //test whether or not we are displaying the very end or beginning of the inventory and display 
    //the navigation arrows accordingly
    private void CheckIfDisplayingLimits()
    {
        if (currentIndex > 0)
        {
            prevButton.gameObject.SetActive(true);
        }
        else
        {
            prevButton.gameObject.SetActive(false);
        }

        if (currentIndex < idsInDisplayOrder.Count - 5)
        {
            nextButton.gameObject.SetActive(true);
        }
        else
        {
            nextButton.gameObject.SetActive(false);
        }
    }

   
    public void Next()
    {
        if (currentIndex < idsInDisplayOrder.Count - 5)
            Display(currentIndex + 1);
    }

    public void Prev()
    {
        if (currentIndex > 0)
            Display(currentIndex - 1);
    }

    
    public void Select(InventorySlot slot)
    {
        selectedSlot = null;
        foreach(InventorySlot i in slots)
        {
            if(i==slot&&i.currentPiece!=null)
            {
                i.Select(true);
                selectedSlot = i;
            }
            else
            {
                i.Select(false);
            }
        }
       
       
        
    }

    public bool RemoveFromInventory(GamePiece gamePiece)
    {
        return RemoveFromInventory(gamePiece.ID);
    }



    //removes one count of the item with the given ID from the inventory 
    //returns true if it was available, false if it wasn't
    public bool RemoveFromInventory(int ID)
    {
        int previousQuantity = 0;
        if (idAmountDictionnary.TryGetValue(ID, out previousQuantity))
        {
            if (previousQuantity > 1)
            {
                idAmountDictionnary[ID]= previousQuantity - 1;
                return true;
            }
            else if (previousQuantity == 1)
            {
                idsInDisplayOrder.Remove(ID);
                idAmountDictionnary.Remove(ID);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    public void AddToInventory(GamePiece gamePiece)
    {
        AddToInventory(gamePiece.ID);
    }
    public void AddToInventory(int ID)
    {
        int previousQuantity = 0;
        if(idAmountDictionnary.TryGetValue(ID,out previousQuantity))
        {
            if (previousQuantity != -1)
            {
                idAmountDictionnary[ID]= previousQuantity + 1;
            }
        }
        else
        {
            idsInDisplayOrder.Add(ID);
            idAmountDictionnary.Add(ID,  1);
        }
        
    }
    // populate the inventory  with amounts[i] pieces of ID[i]
    private void FillInventory(List<int> IDs,List<int> amounts)
    {
        if (IDs.Count != amounts.Count)
        {
            Debug.LogError("list sizes don't match");
            return;
        }
        idsInDisplayOrder = IDs;
        idAmountDictionnary = new Dictionary<int, int>();
        for(int i = 0; i<IDs.Count;i++)
        {
            if (!idAmountDictionnary.ContainsKey(IDs[i]))
            {
                idAmountDictionnary.Add(IDs[i], amounts[i]);
            }

        }
    }
    public void FillInventory(List<int> IDs)
    {
        idsInDisplayOrder = IDs;
        idAmountDictionnary = new Dictionary<int, int>();
        for (int i = 0; i < IDs.Count; i++)
        {
            if (!idAmountDictionnary.ContainsKey(IDs[i]))
            {
                idAmountDictionnary.Add(IDs[i], -1);
            }

        }
    }
    private void FillInventory(List<GamePiece> pieces, List<int> amounts)
    {
        if (pieces.Count != amounts.Count)
        {
            Debug.LogError("list sizes don't match");
            return;
        }
        List<int> IDs = new List<int>();
        foreach(GamePiece gp in pieces)
        {
            IDs.Add(gp.ID);
        }
        FillInventory(IDs, amounts);
    }
    public void FillInventory(List<GamePiece> pieces)
    {
       
        List<int> IDs = new List<int>();
        foreach (GamePiece gp in pieces)
        {
            IDs.Add(gp.ID);
        }
        FillInventory(IDs);
    }

    public void Clear()
    {
        if(idsInDisplayOrder!=null)
             idsInDisplayOrder.Clear();

        if(idAmountDictionnary!=null)
            idAmountDictionnary.Clear();
        
    }

    public List<int> GetIdList()
    {
        List<int> result = new List<int>();
      
        foreach (KeyValuePair<int, int> keyValue in idAmountDictionnary)
        {
            for (int i = 0; i < keyValue.Value; i++)
            {
                result.Add(keyValue.Key);
            }
        }
        return result;
    }

}

