﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public Button testAndValidationButton;
    public Button editLevelButton;
    public Inventory primaryInventory;
    public Inventory secondaryInventory;
    public RadialMenu radialMenu;
    private InventorySlot selectedSlot;
    private bool radialMenuActive;
    public float radialMenuPopupTime;
    public HexGrid grid;
    GameMaster gameMaster;
    // Start is called before the first frame update
    void Start()
    {
        gameMaster = GameMaster.Master;
        HideAll();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Display() {
        primaryInventory.Display();
        secondaryInventory.Display();
    }

    public void HideAll()
    {
        primaryInventory.gameObject.SetActive(false);
        secondaryInventory.gameObject.SetActive(false);
        testAndValidationButton.gameObject.SetActive(false);
        editLevelButton.gameObject.SetActive(false);

    }

    public void Show()
    {
        primaryInventory.gameObject.SetActive(true);
        if (gameMaster.GetMode() == Mode.build)
        {
            secondaryInventory.gameObject.SetActive(true);

        }
        else
        {
            secondaryInventory.gameObject.SetActive(false);
        }
    }

    public void InitializePlayMode(List<int> IDs)
    {
        testAndValidationButton.gameObject.SetActive(false);

        primaryInventory.Clear();
        primaryInventory.idsInDisplayOrder = new List<int>();
        primaryInventory.idAmountDictionnary = new Dictionary<int, int>();

        foreach (int id in IDs)
        {
            primaryInventory.AddToInventory(id);
        }

        primaryInventory.Display();
        secondaryInventory.gameObject.SetActive(false);


    }


    public void InitializeValidationMode(List<int> IDs)
    {
        InitializePlayMode(IDs);
        editLevelButton.gameObject.SetActive(true);

    }
    public void InitializeBuilderMode()
    {
        editLevelButton.gameObject.SetActive(false);
        testAndValidationButton.gameObject.SetActive(true);
        primaryInventory.Clear();

        primaryInventory.FillInventory(gameMaster.existingPieces.AllAvailablePieces());

        primaryInventory.Display();

        secondaryInventory.Clear();
        secondaryInventory.FillInventory(new List<GamePiece>());
       
        secondaryInventory.Display();
    }


    public void DisplayRadialMenuOn(HexCell hc)
    {
        //Debug.Log("popup");
        if (hc != null)
        {

            radialMenu.HookTo(hc);
            radialMenuActive = true;
        }

    }
    public void ClickedOnHexCell(HexCell hc)
    {
        HideRadialMenu();
        if (selectedSlot != null)
        {
            PlaceGamePiece(hc, selectedSlot);

        }
        else
        {
            if (gameMaster.GetMode() == Mode.build)
            {
                hc.SetActive(!hc.IsActive());
            }
        }
    }

    public void HideRadialMenu()
    {

        radialMenu.Deactivate();
        radialMenuActive = false;
    }
    private void PlaceGamePiece(HexCell targetCell, InventorySlot slot)
    {
        if (targetCell != null)
        {
            if (targetCell.isAvailable())
            {
                GamePiece gp = Instantiate(slot.currentPiece);
                gp.PlaceOn(targetCell);

                slot.inventory.RemoveFromInventory(slot.currentPiece);
                //deselect Everything
                slot.inventory.Display();
                Select(null);
            }
        }

    }

    public void Select(InventorySlot slot)
    {
      //  Debug.Log("selecting" + slot);
        selectedSlot = null;
        primaryInventory.Select(slot);
        secondaryInventory.Select(slot);

        if(slot!=null && slot.currentPiece != null)
        {
            selectedSlot = slot;
        }
        

        grid.HighlightAvailableCells(selectedSlot != null);

    }


}
