﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//script for each individual inventory Slots
public class InventorySlot : MonoBehaviour,IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public int slotNumber;
    public Inventory inventory;
    public InventoryManager inventoryManager;
    public GamePiece currentPiece;
    private int quantity;
    public Text amountText;
    public Image gamePieceImage;
    public Image backGroundImage;
    public Image highlighter;

    private GamePiece draggedPiece;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //display the given GamePiece and its quantity
    //gp=null empties the slot and availableQuantity<0 removes the amount Text (for level building)
    public void SetPiece(GamePiece gp,int availableQuantity)
    {
        quantity = availableQuantity;
        currentPiece = gp;
        if (gp != null) {
            gamePieceImage.enabled = true;
            if (gp.inventoryDisplaySprite != null)
            {
                gamePieceImage.sprite=gp.inventoryDisplaySprite;
                gamePieceImage.color = Color.white;

            }
            else
            {

                gamePieceImage.sprite = gp.spriteRenderer.sprite;
            gamePieceImage.color = gp.spriteRenderer.color;
            }
            if (availableQuantity >= 0)
            {
                amountText.text = availableQuantity.ToString();
                amountText.enabled = true;

            }
            else
            {
                amountText.enabled = false;
            }
        }
        else
        {
            gamePieceImage.enabled = false;
            gamePieceImage.sprite = null;
            amountText.enabled = false;
        }
    }

    
    //toggles higlight on the current inventory slot 
    public void Select(bool t)
    {
        GameMaster.Master.inventory.HideRadialMenu();
        highlighter.gameObject.SetActive(t);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        inventoryManager.Select(this);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (currentPiece != null)
        {
            inventoryManager.Select(this);
            SetPiece(currentPiece, quantity - 1);
            draggedPiece = Instantiate(currentPiece);
            draggedPiece.transform.position = GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position)+new Vector3(0,0,9);
        }     
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (draggedPiece != null)
        {
            draggedPiece.DragTo( GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position));
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("End drag for Slot:" + this);
        if (draggedPiece != null)
        {
            if (GameMaster.Master.GetMode() == Mode.build && GameMaster.Master.utilty.IsOverSecondaryInventory(eventData.position))
            {
                DropDraggedToSecondInv();
                return;
            }
            tryToPlace(GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position));
        } 
    }

    private void DropDraggedOn(HexCell hc)
    {
       // Debug.Log("dropping");
        draggedPiece.PlaceOn(hc);
        
        inventory.RemoveFromInventory(draggedPiece);

        draggedPiece = null;
        
        inventory.Display();
  
    }
    private void DropDraggedToSecondInv()
    {
    
            inventory.RemoveFromInventory(draggedPiece);
            inventoryManager.secondaryInventory.AddToInventory(draggedPiece);
            Destroy(draggedPiece.gameObject);
            draggedPiece = null;
            inventoryManager.Display();
            inventoryManager.Select(null);
        
    }
    private void tryToPlace(Vector2 pos)
    {
       // Debug.Log("Try to place");
       
        
        HexCell closestCell = GameMaster.Master.utilty.FindClosestFreeCell(pos);                
        if (closestCell != null)
        {
            DropDraggedOn(closestCell);
        }
        else
        {
            AbortDragAndDrop();
        }
        inventoryManager.Select(null);

    }

    private void AbortDragAndDrop()
    {
        SetPiece(currentPiece, quantity +1);
        Destroy(draggedPiece.gameObject);
        draggedPiece = null;
        

    }
 
}
