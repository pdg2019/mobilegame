﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RadialMenu : MonoBehaviour
{
    public InventoryManager inventory;
    
    private bool active;
    public HexCell hostCell;
    public GameObject radialMenuPanel;
    public GameObject deleteButton;
    public ColorPicker colorPicker;

    // Start is called before the first frame update
    void Start()
    {
        
    }


 
    public void UnHook()
    {
        if (hostCell != null)
        {
            Deactivate();
            hostCell = null;
           
        }
    }
    public bool isActive()
    {
        return active;
    }
    public void HookTo(HexCell hc)
    {
        transform.position = hc.transform.position;
        transform.Translate(0, 0, -1);
        hostCell = hc;
        Activate();
            
    }
    public void Deactivate()
    {
        if (active)
        {
           
            SetColorPickerActive(false);
            radialMenuPanel.SetActive(false);
            active = false;
        }
    }
    public void Activate()
    {
        if (hostCell != null)
        {
            SetColorPickerActive(true);
            radialMenuPanel.SetActive(true);
            active = true;
        }
    }

    public void SelectColor(LaserColor laserColor)
    {
        hostCell.piece.ChangeColor(laserColor);
    }
    public void EnterRotationMode()
    {
        SetColorPickerActive(false);
        deleteButton.SetActive( false);
        
    }
    public void ExitRotationMode()
    {
        SetColorPickerActive(true);

        deleteButton.SetActive(true);
    }

    private void SetColorPickerActive(bool b)
    {
        if(hostCell.piece!=null && hostCell.piece.isColorable())
        {
            colorPicker.RefreshColor();
            colorPicker.gameObject.SetActive(b);
        }
        else
        {
            colorPicker.gameObject.SetActive(false);
        }
    }
    public void RemoveHostPiece()
    {
        if (hostCell != null)
        {

            GamePiece removedPiece = hostCell.piece ;
          
            if (removedPiece != null)
            {
                removedPiece.RemoveFromHost();
                inventory.primaryInventory.AddToInventory(removedPiece);
                Destroy(removedPiece.gameObject);
                inventory.Display();
                
            }
            inventory.HideRadialMenu();
        }
    }
}
