﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

public class RotationArrow : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public RadialMenu radialMenu;
    public Transform anchor;
    private float originalAngle;
    private float originalDistance;
    private float angle;
    private float previousAngle;

    // Start is called before the first frame update
    void Start()
    {
        originalAngle =   anchor.localRotation.eulerAngles.z;
        Debug.Log(originalAngle);
        originalDistance = transform.localPosition.x;
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        if (radialMenu.hostCell.piece != null)
        {
            previousAngle= radialMenu.hostCell.piece.transform.localEulerAngles.z;
        }
        radialMenu.EnterRotationMode();
    }

    public void OnDrag(PointerEventData eventData)
    {
        drag(GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position));
    }

    public void OnEndDrag(PointerEventData eventData)
    {

        radialMenu.ExitRotationMode();
        anchor.rotation = Quaternion.Euler(0, 0, originalAngle);
        transform.localPosition = new Vector3(originalDistance, 0, 0);
        
    }
    private void drag(Vector2 pos)
    {
        Vector2 dir = pos -(Vector2) anchor.position;

        angle = Vector2.Angle(Vector2.down, dir);
        if (dir.x < 0)
        {
            angle = -angle;
        }        
        anchor.rotation = Quaternion.Euler(0, 0, angle+originalAngle);
        transform.localPosition = new Vector3(Mathf.Max( dir.magnitude,originalDistance),0,0);

        if (radialMenu.hostCell.piece != null)
        {
           
            radialMenu.hostCell.piece.setRotation(previousAngle+angle);
                
        }
    }
}
