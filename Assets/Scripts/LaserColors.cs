﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserColors : MonoBehaviour
{

    public Color red;
    public Color redOff;
    public Color green;
    public Color greenOff;
    public Color blue;
    public Color blueOff;
    public Color yellow;
    public Color yellowOff;
    public Color cyan;
    public Color cyanOff;
    public Color magenta;
    public Color magentaOff;
    public Color white;
    public Color whiteOff;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public Color GetTrueColor(LaserColor lc)
    {
        switch (lc)
        {
            case LaserColor.red:return red;
            case LaserColor.green: return green;
            case LaserColor.blue: return blue;
            case LaserColor.yellow: return yellow;
            case LaserColor.cyan: return cyan;
            case LaserColor.magenta: return magenta;
            case LaserColor.white:return white;
            default: return Color.gray;
        }
    }

    public Color GetTrueColorOff(LaserColor lc)
    {
        switch (lc)
        {
            case LaserColor.red: return redOff;
            case LaserColor.green: return greenOff;
            case LaserColor.blue: return blueOff;
            case LaserColor.yellow: return yellowOff;
            case LaserColor.cyan: return cyanOff;
            case LaserColor.magenta: return magentaOff;
            case LaserColor.white: return whiteOff;
            default: return Color.gray;
        }
    }

    public static List<LaserColor> DecomposeColor(LaserColor col)
    {
        List<LaserColor> result = new List<LaserColor>();
        switch (col)
        {
            case LaserColor.red:
                result.Add(LaserColor.red);
                break;
            case LaserColor.green:
                result.Add(LaserColor.green);
                break;
            case LaserColor.blue:
                result.Add(LaserColor.blue);
                break;
            case LaserColor.yellow:
                result.Add(LaserColor.red);
                result.Add(LaserColor.green);
                break;
            case LaserColor.cyan:
                result.Add(LaserColor.green);
                result.Add(LaserColor.blue);
                break;
            case LaserColor.magenta:
                result.Add(LaserColor.red);
                result.Add(LaserColor.blue);
                break;
            case LaserColor.white:
                result.Add(LaserColor.red);
                result.Add(LaserColor.green);
                result.Add(LaserColor.blue);
                break;        
        }
        return result;
    }
    public static LaserColor ComputeColorMix(List<LaserColor> colors)
    {
        if (colors.Count == 1)
        {
            return colors[0];
        }
        else if (colors.Count == 2)
        {
            if (colors[0] == colors[1])
            {
                return colors[0];
            }

            if (colors[0] == LaserColor.red)
            {
                return MixWithRed(colors[1]);
            }
            else if (colors[0] == LaserColor.blue)
            {
                return MixWithBlue(colors[1]);
            }
            else if (colors[0] == LaserColor.green)
            {
                return MixWithGreen(colors[1]);
            }

            if (colors[1] == LaserColor.red)
            {
                return MixWithRed(colors[0]);
            }
            else if (colors[1] == LaserColor.blue)
            {
                return MixWithBlue(colors[0]);
            }
            else if (colors[1] == LaserColor.green)
            {
                return MixWithGreen(colors[0]);
            }

        }
        else if (colors.Count == 3)
        {
            List<LaserColor> firstColors = new List<LaserColor>();
            firstColors.Add(colors[0]);
            firstColors.Add(colors[1]);
            LaserColor firstMix = ComputeColorMix(firstColors);
            firstColors.Clear();
            firstColors.Add(firstMix);
            firstColors.Add(colors[2]);
            return ComputeColorMix(firstColors);
        }
        return LaserColor.undefined;
    }
    private static LaserColor MixWithRed(LaserColor c)
    {
        switch (c)
        {
            case LaserColor.red: return LaserColor.red;
            case LaserColor.blue: return LaserColor.magenta;
            case LaserColor.green: return LaserColor.yellow;
            case LaserColor.cyan: return LaserColor.white;
            default: return LaserColor.undefined;

        }
    }

    private static LaserColor MixWithGreen(LaserColor c)
    {
        switch (c)
        {
            case LaserColor.red: return MixWithRed(LaserColor.green);
            case LaserColor.blue: return LaserColor.cyan;
            case LaserColor.green: return LaserColor.green;
            case LaserColor.magenta: return LaserColor.white;
            default: return LaserColor.undefined;

        }
    }
    private static LaserColor MixWithBlue(LaserColor c)
    {
        switch (c)
        {
            case LaserColor.red: return MixWithRed(LaserColor.blue);
            case LaserColor.blue: return LaserColor.blue;
            case LaserColor.green: return MixWithGreen(LaserColor.blue);
            case LaserColor.yellow: return LaserColor.white;
            default: return LaserColor.undefined;

        }
    }


}

public enum LaserColor { red,green,blue,yellow,cyan,magenta,white,undefined}