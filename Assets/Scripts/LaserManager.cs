﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserManager : MonoBehaviour
{
    public Laser laserPrefab;
    private List<Laser> activeLasers = new List<Laser>();
    private List<LaserGenerator> generators = new List<LaserGenerator>();


   
    public void Reset()
    {
        foreach (Laser l in activeLasers)
        {
            l.Clear();
        }
      activeLasers= new List<Laser>();
        generators = new List<LaserGenerator>();
    }
    public void UpdateAll()
    {
        foreach(Laser l in activeLasers)
        {
            l.Clear();
        }
        foreach(LaserGenerator g in generators)
        {
            g.BuildLaser();
        }
    }
    // Update is called once per frame
  
    public void RemoveGenerator(LaserGenerator g)
    {
        if (generators.Contains(g))
        {
            generators.Remove(g);
        }
    }
    public void AddGenerator(LaserGenerator g)
    {
        generators.Add(g);
    }
    public Laser newLaser(){
      Laser newLaser = Instantiate(laserPrefab);
      activeLasers.Add(newLaser);
      return newLaser;
    }
}
