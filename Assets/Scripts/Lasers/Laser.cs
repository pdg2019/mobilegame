﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public LaserSegment laserSegmentPrefab;
    public List<LaserSegment> segments;

    private List<GamePiece> piecesOnTrajectory;
    private LaserColor color;

    public GamePiece start;
    public bool ended;
    public float startAngle;
    // Start is called before the first frame update
    void Start()
    {
        
        //Clear();
        //BuildLaser(start, startAngle );
    }



    public void Clear()
    {
        foreach(LaserSegment ls in segments)
        {
            ls.Clear();
        }
        segments = new List<LaserSegment>();
        if (piecesOnTrajectory == null)
        {
            piecesOnTrajectory = new List<GamePiece>();
        }
        foreach(GamePiece  gp in piecesOnTrajectory)
        {
            gp.RemoveIncomingLaser(this);
        }

        piecesOnTrajectory = new List<GamePiece>();
    }


    public void BuildLaser(GamePiece startPiece,float angle)
    {
        start = startPiece;
       // Debug.Log("building laser" + gameObject.ToString());
        Clear();
        //AddPiece(startPiece);
        GamePiece currentPiece = startPiece;
        Vector2 dir = (Quaternion.AngleAxis(angle, Vector3.forward) * Vector2.right).normalized;
        bool continueBuilding = true;
        int reflection = 0;
        while (continueBuilding)
        {
                    
            reflection++;
            
            GamePiece gp = ScanForNextPiece(currentPiece, dir);
            LaserSegment nextSegment = LaserSegmentPool.laserSegmentPool.GetSegment();
            if (gp == null)
            {
              
                continueBuilding = false;
                nextSegment.Setup(color, currentPiece.position, dir);
            }
            else
            {
              //  gp.AddIncomingLaser(l)
                
                AddPiece(gp);
                gp.AddIncomingLaser(this, dir);
                Vector2 reflectionDir = gp.ComputeReflection(dir);
                nextSegment.Setup(color,currentPiece.position, gp.position);

                if (reflectionDir == Vector2.zero)
                {
                    continueBuilding = false;
                }
                else
                {
                    dir = reflectionDir;
                    currentPiece = gp;
                }

            }
            segments.Add(nextSegment);
            if (reflection > 1000)
            {
                continueBuilding = false;
            }

        }

    }

    Vector2 EndPoint()
    {
        return Vector2.zero;
    }
    GamePiece ScanForNextPiece(GamePiece current,Vector2 dir)
    {

        Vector2 raycastStart =(Vector2) current.transform.position + dir.normalized * HexMetrics.shortRadius / 2;
        RaycastHit2D hit = Physics2D.Raycast(raycastStart, dir,100,GameMaster.Master.gamePiecesMask);
        if (hit.collider != null)
        {
            GamePiece hitPiece = hit.collider.gameObject.GetComponentInParent<GamePiece>();
            return hitPiece;
        }
        return null;
    }
    void AddPiece(GamePiece p)
    {
        piecesOnTrajectory.Add(p);
       
    }
 

    public void SetColor(LaserColor c)
    {
        color = c;
       
     
    }

    public LaserColor GetColor()
    {
        return color;
    }
}
