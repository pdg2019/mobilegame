﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class LaserSegment : MonoBehaviour
{
    public float textureScrollSpeed;
    public float width;
    HexCell origin;
    HexCell destination;
    Vector2 dir;
    Vector2 normal;

    Mesh mesh;
    MeshRenderer mr;
    private bool active= false;
    private float prevOffset;
    private LaserColor color;
    
    public void Start()
    {
        mr = GetComponent<MeshRenderer>();
        mr.sortingLayerName = "Lasers";
        mr.sortingOrder = 0;
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh= mesh;
    }

    public void Update()
    {
        if (active)
        {
            float newOffset = prevOffset - textureScrollSpeed * Time.deltaTime;
            mr.material.SetTextureOffset("_MainTex", new Vector2(newOffset,0));
            prevOffset = newOffset;
        }
    }
    public void Setup(LaserColor lc,HexCell ori,HexCell dest)
    {
        color = lc;
        origin = ori;
        destination = dest;
        dir = dest.transform.position - ori.transform.position;
        normal = (Quaternion.AngleAxis(90, Vector3.forward) * dir).normalized;
        GenerateMesh();
    }
    public void Setup(LaserColor lc, HexCell ori, Vector2 d)
    {
        color = lc;
        origin = ori;
        destination = null;
        dir = d;
        normal = (Quaternion.AngleAxis(90, Vector3.forward) * dir).normalized;
        GenerateMesh();
    }
    private void GenerateMesh()
    {
        mesh.name = "Procedural Grid";

        Vector3[] vertices = new Vector3[4];
        Vector2[] uv = new Vector2[vertices.Length];
        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);

        vertices[0] = origin.transform.position+(Vector3)normal*width;
       


        vertices[2] = origin.transform.position - (Vector3)normal * width;
        if(destination!= null)
        {
            vertices[1] = destination.transform.position + (Vector3)normal * width;
            vertices[3] = destination.transform.position -(Vector3)normal * width;
        }
        else
        {
            vertices[1] = origin.transform.position + (Vector3)normal * width + (Vector3) dir*50f;
            vertices[3] = origin.transform.position - (Vector3)normal * width + (Vector3)dir * 50f;
        }

        mesh.vertices = vertices;
        int[] triangles = new int[6];
        triangles[0] = 0;
        triangles[1] =1;
        triangles[2] = 2;
        triangles[3] = 1;
        triangles[4] =3;
        triangles[5] =2;

        mesh.uv = uv;
        mesh.triangles = triangles;
        float length = (vertices[1] - vertices[0]).magnitude;
        mr.material.mainTextureScale = new Vector2(length, 1);
        active = true;
        Color c = GameMaster.Master.laserColors.GetTrueColor(color);
        float h;
        float s;
        float v;
        Color.RGBToHSV(c, out h, out s, out v);
        Color ec = Color.HSVToRGB(h, s/2f, v  );
        mr.material.SetColor("_EmissionColor", ec);
        mr.material.color=c;

    }

    public void SetColor(Color c)
    {
        mr.material.color = c;
    }
    public void Clear()
    {
        active = false;
        mesh.triangles = null;
        mesh.vertices = null;
        LaserSegmentPool.laserSegmentPool.PoolSegment(this);
    }
}
