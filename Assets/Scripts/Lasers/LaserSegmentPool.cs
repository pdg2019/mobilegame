﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserSegmentPool : MonoBehaviour
{
    public static LaserSegmentPool laserSegmentPool ; 
    public LaserSegment prefab;
    public Queue<LaserSegment> availableSegments;

    public void Start()
    {
        if(laserSegmentPool == null)
        {
            laserSegmentPool = this;
        }
        availableSegments = new Queue<LaserSegment>();
        for(int i = 0; i< 50; i++)
        {
            LaserSegment ls = Instantiate(prefab);
            ls.gameObject.transform.SetParent(this.transform);
           
            PoolSegment(ls);
        }
    }
    public LaserSegment GetSegment()
    {
        if (availableSegments.Count == 0)
        {
            LaserSegment ls = Instantiate(prefab);
            ls.gameObject.transform.SetParent(this.transform);
            return ls;
        }
        else
        {
            return availableSegments.Dequeue();
        }
    }

    public void PoolSegment(LaserSegment ls)
    {
        availableSegments.Enqueue(ls);
    }
}
