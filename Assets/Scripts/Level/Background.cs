﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Background : MonoBehaviour, IPointerDownHandler
{


    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("clicked on bg");

        GameMaster.Master.inventory.HideRadialMenu();
        GameMaster.Master.inventory.Select(null);
    }



    // Start is called before the first frame update
    void Start()
    {
         
    }

}
