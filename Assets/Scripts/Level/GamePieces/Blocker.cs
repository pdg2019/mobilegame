﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocker : GamePiece
{
    public override Vector2 ComputeReflection(Vector2 inputDir)
    {

        return Vector2.zero;
    }
}
