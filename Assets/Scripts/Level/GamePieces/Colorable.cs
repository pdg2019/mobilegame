﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Colorable : GamePiece
{
    protected LaserColor color =LaserColor.white;

    // Start is called before the first frame update
    public LaserColor GetLaserColor()
    {
        return color;
    }
    public override bool isColorable()
    {
        return true;
    }
    // Update is called once per frame
  
    public virtual void SetColor(LaserColor c)
    {
        color = c;
        spriteRenderer.color = gameMaster.laserColors.GetTrueColor(c);
       
    }
}
