﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

[RequireComponent(typeof(SpriteRenderer))]

public class GamePiece : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,  IPointerDownHandler, IPointerUpHandler

{

    public Sprite inventoryDisplaySprite;
    public bool fixedPiece = false;
    public int rotationStep=60;
    public GameMaster gameMaster;
    public int ID;
    public SpriteRenderer spriteRenderer;
    public Collider2D laserCollider;
    public HexCell position = null;
    private bool dragged=true;
    private HexCell previousHostCell;
    private Vector2 targetPosition;
    public int orientation = 0;
    private Laser[] outputs = new Laser[6];
    protected Laser[] inputs = new Laser[6];

    private float startedPressTime;
    private bool beingClicked = false;
    // Start is called before the first frame update
    public virtual void Start()
    {

        if (laserCollider == null)
        {
            laserCollider = GetComponentsInChildren<Collider2D>()[1];
        }
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        spriteRenderer.sortingLayerName = "DraggedGamePieces";
        gameMaster = GameMaster.Master;
    }



    // Update is called once per frame
    public void Update()
    {

        if (dragged)
        {
            Vector2 newPos = Vector2.Lerp(transform.position, targetPosition, 0.87f);
            transform.position = newPos;
        }
        else
        {
            if (beingClicked)
            {
                if (Time.time >= startedPressTime + gameMaster.inventory.radialMenuPopupTime)
                {
                    if (!fixedPiece || GameMaster.Master.GetMode() == Mode.build)
                    {
                        gameMaster.inventory.DisplayRadialMenuOn(position);
                    }
                }
            }
        }
    }
    public void SetFixed(bool t)
    {
        fixedPiece = t;

    }
    public void DragTo(Vector2 pos)
    {
        targetPosition = pos;
    }
    public void RotateStep()
    {
        setRotation(transform.rotation.eulerAngles.z-rotationStep);
     
    }
    public virtual void PlaceOn(HexCell hc)
    {
        if(GameMaster.Master.GetMode()== Mode.build)
        {
            fixedPiece = true;
        }
        if (laserCollider == null)
        {
            laserCollider = GetComponentsInChildren<Collider2D>()[1];
        }

        laserCollider.enabled = true;
       // Debug.Log("Placed On" + hc);
        spriteRenderer.sortingLayerName = "GamePieces";
        dragged = false;
        position = hc;
        transform.position = hc.transform.position+new Vector3(0,0,-1);
        hc.Place(this);
        GameMaster.Master.inventory.Select(null);
        UpdateAllLasers();
    }

    public virtual void RemoveFromHost()
    {
        laserCollider.enabled = false;
        position.RemovePiece();
        UpdateAllLasers();
    }

    public void Remove()
    {
        Destroy(gameObject);
        UpdateAllLasers();
    }

    public virtual Vector2 ComputeReflection(Vector2 inputDirection)
    {
        
        return Vector2.zero;
	}

    public virtual void setRotation(float angle)
    {

        int numberOfTurn = Mathf.FloorToInt(angle / 360f);
        angle = angle -360*numberOfTurn;
        float x = angle / rotationStep;
        x = Mathf.Round(x);

        float snappedAngle = x * rotationStep;
        if (snappedAngle < 0)
        {
            snappedAngle = 360 + snappedAngle;
        }
        if (snappedAngle == 360)
        {
            snappedAngle = 0;
        }
        if (orientation != snappedAngle)
        {
            orientation = (int)snappedAngle;
            transform.localRotation = Quaternion.Euler(0, 0, snappedAngle);
            UpdateAllLasers();
        }
        
    }



    public void OnBeginDrag(PointerEventData eventData)
    {
        if(!fixedPiece|| GameMaster.Master.GetMode() == Mode.build)
        {
            previousHostCell = position;
            spriteRenderer.sortingLayerName = "DraggedGamePieces";
            RemoveFromHost();
            dragged = true;
            GameMaster.Master.inventory.grid.HighlightAvailableCells(true);
            GameMaster.Master.inventory.HideRadialMenu();
            DragTo(GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position));
        }
    }

    public void OnDrag(PointerEventData eventData)
    {

        if (!fixedPiece || GameMaster.Master.GetMode() == Mode.build)
        {
            DragTo(GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position));
        }
    }

    private void DropDraggedToSecondInv()
    {
        InventoryManager inventoryManager = GameMaster.Master.inventory;
        inventoryManager.secondaryInventory.AddToInventory(this);
  
        inventoryManager.Display();
        inventoryManager.Select(null);
        Destroy(gameObject);
    }

    public void OnEndDrag(PointerEventData eventData)
    {

        if (!fixedPiece || GameMaster.Master.GetMode() == Mode.build)
        {
            // Debug.Log("End drag for Slot:" + this);
            dragged = false;

            if (GameMaster.Master.GetMode() == Mode.build && GameMaster.Master.utilty.IsOverSecondaryInventory(eventData.position))
            {

                DropDraggedToSecondInv();
                return;
            }
            HexCell closestCell = GameMaster.Master.utilty.FindClosestFreeCell(GameMaster.Master.MainCamera().ScreenToWorldPoint(eventData.position));
            if (closestCell == null)
            {
                gameMaster.inventory.primaryInventory.AddToInventory(this);
                gameMaster.inventory.Display();
                gameMaster.inventory.Select(null);
                Remove();
            //    PlaceOn(previousHostCell);
            }
            else
            {
                PlaceOn(closestCell);
            }
            GameMaster.Master.inventory.grid.HighlightAvailableCells(false);
        }
    }

    public virtual bool isColorable()
    {
        return false;
    }
    public virtual void ChangeColor(LaserColor color)
    {
        return;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        beingClicked = true;
        startedPressTime = Time.time;
        

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        beingClicked = false;
        if(Time.time <= startedPressTime + gameMaster.inventory.radialMenuPopupTime)
        {
            if (!position.RadialMenuHookedHere())
            {

                if (!fixedPiece || GameMaster.Master.GetMode() == Mode.build)
                {
                    RotateStep();
                    GameMaster.Master.inventory.HideRadialMenu();
                }
            }
            else
            {
                GameMaster.Master.inventory.HideRadialMenu();
            }


        }
    }

    public void UpdateAllLasers()
    {
        if (gameMaster == null)
        {
            gameMaster = GameMaster.Master;
        }
        gameMaster.laserManager.UpdateAll();
    }
    public void clearIncomingLasers()
    {
        for (int i = 0; i < inputs.Length; i++)
        {
            inputs[i] = null;
        }
    }
    public void AddIncomingLaser(Laser l, Vector2 dir)
    {
        int angle = Utility.AngleFromDir(dir);
        if (angle > 180)
        {
            angle = angle - 180;
        }
        else
        {
            angle = angle + 180;
        }
        if(angle<30|| angle > 330)
        {
            inputs[0] = l;
        }else if (angle < 90)
        {
            inputs[1] = l;
        }
        else if (angle < 150)
        {
            inputs[2] = l;
        }
        else if (angle < 210)
        {
            inputs[3] = l;
        }
        else if (angle < 270)
        {
            inputs[4] = l;
        }
        else if (angle <= 330)
        {
            inputs[5] = l;
        }
        ComputeLasers(l);

    }
    public void RemoveIncomingLaser(Laser l)
    {
       // Debug.Log("removing Laser from:" + gameObject.ToString());
        for(int i=0;i< inputs.Length; i++)
        {
            if (l ==inputs[i])
            {
                inputs[i] = null;
            }
           
        }
        ComputeLasers(l);

    }
    public virtual void ComputeLasers(Laser last)
    {
        return;
    }
}
