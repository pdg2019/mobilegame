﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LaserGenerator : Colorable, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
  
    private Laser laser ;
    // Update is called once per frame
    public override void Start()
    {
        base.Start();
        SetColor(color);
    }
    public new void Update()
    {
        base.Update();
    }

    public override void RemoveFromHost()
    {
        laser.Clear();
        gameMaster.laserManager.RemoveGenerator(this);
        base.RemoveFromHost();
       
            
        
    }

    public override void ChangeColor(LaserColor lc)
    {
        SetColor(lc);
    }
    public override void setRotation(float angle)
    {
        
        base.setRotation(angle);
        laser.BuildLaser(this, orientation);


    }
    public override void PlaceOn(HexCell hc){

       // Debug.Log("LaserGen PlaceOn called");

        if (gameMaster == null)
        {
            gameMaster = GameMaster.Master;
        }
        gameMaster.laserManager.AddGenerator(this);
        if (laser == null)
        {
            laser = gameMaster.laserManager.newLaser();
            laser.SetColor(color);
        }
        base.PlaceOn(hc);
        
          laser.BuildLaser(this, orientation);
    }

    public override void SetColor(LaserColor c)
    {
        base.SetColor(c);
        if (laser != null)
        {
            laser.SetColor(color);
            BuildLaser();
        }
    }
    public void ClearLaser()
    {
        if (laser != null)
        {
            laser.Clear();
        }
    }
    public void BuildLaser()
    {
        if (position != null)
        {
            laser.BuildLaser(this,orientation);
        }
    }
}
