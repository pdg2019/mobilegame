﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserReceiver : Colorable
{
    
    public  bool activated= false;
    // Start is called before the first frame update

    // Update is called once per frame
    public override void Start()
    {
        base.Start();
        SetColor(color);
    }
    public new void Update()
    {
        base.Update();
    }


    public override void ChangeColor(LaserColor lc)
    {
        SetColor(lc);
        ComputeLasers(null);
    }
    public override void ComputeLasers(Laser l)
    {
        int rotationStep = orientation / 60;
        if (inputs[rotationStep]!=null && inputs[rotationStep].GetColor() == color)
        {
            SetActive(true);
        }
        else
        {
            SetActive(false);
        }
        SetColor(color);
    }

    private void SetActive(bool b)
    {
        if (activated != b)
        {
            activated = b;
            gameMaster.currentLevel.UpdateStatus();
        }
    }
    public override void SetColor(LaserColor c)
    {
        color = c;
        if (spriteRenderer != null)
        {
            if (activated)
            {
                spriteRenderer.color = gameMaster.laserColors.GetTrueColor(c);
            }
            else
            {
                spriteRenderer.color = gameMaster.laserColors.GetTrueColorOff(c);
            }
        }
     
    }


}
