﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mixer : GamePiece
{
    public List<MixerPiece> pieces;
    
    public LaserColor coreColor;
    protected bool emitting;
    public override void Start()
    {
        base.Start();
        if (pieces.Count != 6)
        {
            throw new System.Exception("A Mixer needs 6 pieces");
        }
    }

    public override void ComputeLasers(Laser l)
    {
        
        int rotationStep = orientation / 60;

        for(int i=0; i < inputs.Length; i++)
        {
       
            int relativeIndex = (6+i - rotationStep)%6;
            
            if (  pieces[relativeIndex].type == MixerPieceType.Receiver)
            {
                if (inputs[i] != null)
                {
                    pieces[relativeIndex].SetColor(inputs[i].GetColor());
                    pieces[relativeIndex].SetActive(true);
                }
                else
                {
                    pieces[relativeIndex].SetColor(LaserColor.undefined);
                    pieces[relativeIndex].SetActive(false);
                }
                pieces[relativeIndex].UpdateAspect();
            }
        }
        UpdateCoreColor();

    }

    public void UpdateCoreColor()
    {
       // Debug.Log("UpdatingCore");
        List<LaserColor> inputColors = new List<LaserColor>();
        foreach(MixerPiece mp in pieces)
        {
            if(mp.type== MixerPieceType.Receiver)
            {
                if(mp.IsActive())
                {
                    inputColors.Add(mp.GetColor());
                }               
            }
        }

        SetCoreColor(LaserColors.ComputeColorMix(inputColors));
      

    }
    /// <summary>
    /// set the color of the core and updates the sprite color and the emiters/laser if necessary
    /// </summary>
    /// <param name="color"> new coreColor</param>
    private void SetCoreColor(LaserColor color)
    {
       // Debug.Log(color);


        if (color != coreColor)
        {
            coreColor = color;
            if (spriteRenderer != null)
            {
                if (coreColor == LaserColor.undefined)
                {
                    spriteRenderer.color = GameMaster.Master.laserColors.whiteOff;
                    emitting = false;
                }
                else
                {
                    spriteRenderer.color = GameMaster.Master.laserColors.GetTrueColor(coreColor);
                    emitting = true;
                }
            }
           
            UpdateEmittersColors();
        }   
    }

    protected virtual void UpdateEmittersColors()
    {
       
        foreach (MixerPiece mp in pieces)
        {

            if (mp.type == MixerPieceType.Emitter)
            {
                mp.SetActive(emitting);
                if (emitting)
                {
                    mp.SetColor(coreColor);
                }
            }
        }
    }
}
