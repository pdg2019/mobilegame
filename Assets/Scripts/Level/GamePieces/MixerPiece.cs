﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SpriteRenderer))]
public class MixerPiece : MonoBehaviour
{
    public Mixer mixer;
    public SpriteRenderer spriteRenderer;
    public MixerPieceType type;
    private bool active=false;
    public LaserColor color;

    
    private Laser laser;

    public void SetColor(LaserColor c)
    {
        if (color != c)
        {
            color = c;
            UpdateAspect();
            
        }
    }

    public LaserColor GetColor()
    {
        return color;
    }

    public void SetLaser(Laser l)
    {
        laser = l;
    }

    public Laser GetLaser()
    {
        return laser;
    }
    public void SetActive(bool b)
    {
        active = b;
        
        UpdateAspect();
    }

    public bool IsActive()
    {
        return active;
    }

    public void UpdateAspect()
    {
        if (spriteRenderer != null)
        {
            if (active)
            {
                spriteRenderer.color = GameMaster.Master.laserColors.GetTrueColor(color);
            }
            else
            {
                spriteRenderer.color = GameMaster.Master.laserColors.whiteOff;
            }
            UpdateLaser();
        }
       
        
    }

    public void UpdateLaser()
    {

        if(type == MixerPieceType.Emitter)
        {
     //   Debug.Log("Updating Laser in emitter");
            if (active == true)
            {
                if (laser == null)
                {
                   // Debug.Log("Creating Laser");

                    laser = GameMaster.Master.laserManager.newLaser();
                   
                }
              //  Debug.Log("Launching rebuild");
                laser.SetColor(color);
                laser.BuildLaser(mixer, mixer.orientation + transform.localRotation.eulerAngles.z);
            }
            else
            {
                if (laser != null)
                {
                  //  Debug.Log("Clearing Laser");

                    laser.Clear();
                }
            }
        }
        
    }
}

public enum MixerPieceType{Emitter,Receiver,Wall}