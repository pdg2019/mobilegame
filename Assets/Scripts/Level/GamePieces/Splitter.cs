﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splitter : Mixer
{
    private int colorRotation;
    public override void Start()
    {
        base.Start();
        if (pieces.Count != 6)
        {
            throw new System.Exception("A Splitter needs 6 pieces");
        }
    }

    protected override void UpdateEmittersColors()
    {

        List<LaserColor> emitterColors=LaserColors.DecomposeColor(coreColor);
        int count= 0;
        foreach (MixerPiece mp in pieces)
        {

            if (mp.type == MixerPieceType.Emitter)
            {
                mp.SetActive(emitting);
                if (emitting)
                {
                    if (emitterColors.Count > count)
                    {
                        mp.SetColor(emitterColors[count]);
                    }
                    else
                    {
                        mp.SetActive(false);
                    }
                    
                }
                count++;
            }
        }
    }
}
