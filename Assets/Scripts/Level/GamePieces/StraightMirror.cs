﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightMirror : GamePiece
{
    
    public override void Start()
    {
        rotationStep = 30;
        base.Start();
    }

    public override Vector2 ComputeReflection(Vector2 inputDir)
    {
        
        float orientationSin = Mathf.Sin(transform.rotation.eulerAngles.z);
        float orientationCos = Mathf.Cos(transform.rotation.eulerAngles.z);
        
        Vector2 normalDir = (Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * Vector2.up).normalized;
       // Debug.Log("angle"+Mathf.Abs(Vector2.Angle(inputDir, normalDir)));
        int angle = Mathf.RoundToInt(Mathf.Abs(Vector2.Angle(inputDir, normalDir)));
        if (angle ==90)
        {
            return Vector2.zero;
        }
        Vector2 reflection = Vector2.Reflect(inputDir, normalDir);
        return reflection;
    
    }

}
