﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tunnel : GamePiece
{
    public override Vector2 ComputeReflection(Vector2 inputDir)
    {
        float orientationSin = Mathf.Sin(transform.rotation.eulerAngles.z);
        float orientationCos = Mathf.Cos(transform.rotation.eulerAngles.z);

        Vector2 normalDir = (Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * Vector2.right).normalized;
         
        int angle = Mathf.RoundToInt(Mathf.Abs(Vector2.Angle(inputDir, normalDir)));
       
        if (angle ==180 ||angle==0)
        {
            return inputDir;
        }

        else
        {
            return Vector2.zero;
        }

    }
}
