﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelDouble : GamePiece
{
    public override Vector2 ComputeReflection(Vector2 inputDir)
    {
        float orientationSin = Mathf.Sin(transform.rotation.eulerAngles.z);
        float orientationCos = Mathf.Cos(transform.rotation.eulerAngles.z);

        Vector2 rightDir = (Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * Vector2.right).normalized;
        int angle = Mathf.RoundToInt(Vector2.SignedAngle(rightDir, -inputDir));

       // Debug.Log("dir" + rightDir);
        //Debug.Log("angle" + angle);
        if (angle==0||angle==60||angle==180 ||angle==-180 ||angle==-120)
        {
            return inputDir;
        }
        else
        {
            return Vector2.zero;
        }

    }
}
