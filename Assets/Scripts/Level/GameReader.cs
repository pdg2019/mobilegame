﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class GameReader : MonoBehaviour
{
    public HexGrid grid;

    public List<int> getInventory(string json)
    {
        var JsonObject = JSON.Parse(json);
        JSONArray inventory = (JSONArray)JsonObject["inventory"];
        List<int> idList = new List<int>();

        for (int i = 0; i < inventory.Count; i++)
        {
            int pieceID = inventory[i];
            idList.Add(pieceID);
        }
        return idList;
    }

    public void RandomLevelFromDatabase()
    {

    }

    public void Read(string json)
    {
        grid.Clear();
        GameMaster.Master.currentLevel.Reset();
        GameMaster.Master.currentLevel.setJson(json);
        var JsonObject = JSON.Parse(json);
        int height = System.Convert.ToInt32(JsonObject["height"].Value);
        int width = System.Convert.ToInt32(JsonObject["width"].Value);
        JSONArray cellsArray = (JSONArray)JsonObject["cells"];

        grid.Build(width, height);

        for(int i = 0; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                if(i % 2 == 1 && j == width - 1)
                {
                    break;
                }
                HexCell cell = grid.getCell(j, i);
                JSONObject jsonCell = (JSONObject)cellsArray[i][j];

                if (jsonCell["empty"]!=null)
                {
                    cell.RemovePiece();
                }
                else
                {
                    if (jsonCell["inactive"] != null)
                    {
                        cell.SetActive(false);
                    }
                    else
                    {
                        int pieceID = System.Convert.ToInt32(jsonCell["pieceID"].Value);
                        double rotation = System.Convert.ToDouble(jsonCell["orientation"].Value);
                        GamePiece gp = GameMaster.Master.existingPieces.GetPiece(pieceID);

                        gp = Instantiate(gp);
                        gp.PlaceOn(cell);
                        gp.SetFixed(true);
                        gp.setRotation((float)rotation);
                        if (gp.isColorable())
                        {
                            LaserColor color = (LaserColor)jsonCell["color"].AsInt;
                            ((Colorable)gp).SetColor(color);
                        }
                        if (pieceID == 2)
                        {
                            GameMaster.Master.currentLevel.AddReceiver((LaserReceiver)gp);
                        }
                      
                    }       
                }
            }
        }
    }

    public void setGrid(HexGrid grid)
    {
        this.grid = grid;
    }


}
