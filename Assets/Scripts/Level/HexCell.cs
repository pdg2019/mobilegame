﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HexCell : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool active=true;
    public SpriteRenderer myRenderer;
    public SpriteRenderer highlighter;
    public int x;
    public int y;

    public GamePiece piece;

    private HexGrid grid;
  
    
   
	// Use this for initialization
	void Start () {
     
	}

    public HexGrid getGrid()
    {
        if (grid == null)
        {
            grid = GameObject.FindGameObjectWithTag("Grid").GetComponent<HexGrid>();
        }
        return grid;
    }
    public void SetActive(bool t)
    {
        
        active = t;
        if (active)
        {
            myRenderer.enabled = true;
            
            myRenderer.sprite = getGrid().ActiveSprite;
        }
        else
        {
            if (GameMaster.Master.GetMode() == Mode.build)
            {
                myRenderer.enabled = true;
                myRenderer.sprite = getGrid().InactiveSpriteInEditor;
            }
            else
            {
                myRenderer.enabled = false;
            }
        }
       
    }

    internal object GetPiece()
    {
        return piece;
    }

    public bool IsActive() {
        return active;
    }
    public void RotateStep()
    {
        if (piece != null)
        {
            piece.RotateStep();
        }
    }
    public void Highlight(bool t)
    {
        highlighter.enabled = t;
    }
    public bool isAvailable()
    {

        return piece == null && active;
    }
    public void Place(GamePiece gp)
    {
        piece = gp;
    }

    public GamePiece RemovePiece()
    {

        GamePiece removedPiece = null;
        if (piece != null)
        {
            removedPiece = piece;
            piece = null;
        }
        return removedPiece;
    }

    public bool RadialMenuHookedHere()
    {
        return GameMaster.Master.inventory.radialMenu.hostCell == this &&
            GameMaster.Master.inventory.radialMenu.isActive();
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
       
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (piece != null)
        {
            piece.OnPointerDown(eventData);
        }
        else
        {
            
            GameMaster.Master.inventory.ClickedOnHexCell(this);

        }
    }

}
