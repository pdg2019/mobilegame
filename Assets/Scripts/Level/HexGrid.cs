﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class HexGrid : MonoBehaviour
{
    public Sprite ActiveSprite;
    public Sprite InactiveSpriteInEditor;
    public Camera cam;
    public bool generateNew;
   
    public HexCell cellPrefab;
    public List<HexCell> cells;
    public int height;
    public int width;

    Vector3 topRight;
    Vector3 bottomLeft;



    public float trueScale;
    public HexCell[][] cellsArray;
    // Use this for initialization
    void Start()
    {
      if(cam == null)
        {
            cam = Camera.main;
        }
    
    }

    void GenerateMesh(int w, int h, float scale)
    {
        width = w;
        height = h;
        cells = new List<HexCell>();
        int count = 0;
        float xOffset = -(HexMetrics.shortRadius * (width - 1) * 2f) * scale / 2f;
        float yOffset = -HexMetrics.longRadius * (height - 1) * 1.5f * scale / 2f;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                
                float secondRowOffset = (HexMetrics.shortRadius * 2f) * scale / 2;
               


                float x = i * 2 * HexMetrics.shortRadius * scale;
                float y = j * 1.5f * HexMetrics.longRadius * scale;

                if(!(j%2 ==1 && i== width - 1))
                {
                    cells.Add(GameObject.Instantiate(cellPrefab.gameObject, new Vector3(x +xOffset, y+ yOffset, 0 ), Quaternion.identity).GetComponent<HexCell>());
                    cells[count].transform.parent = transform;
                    cells[count].x = i;
                    cells[count].y = j;
                   
                    cells[count].transform.localScale = new Vector3(trueScale*scale, trueScale * scale, trueScale * scale);
                    cells[count].transform.localScale = new Vector3(trueScale, trueScale, trueScale);
                    



                    if ((j % 2 == 1))
                    {
                        cells[count].transform.position = new Vector3(x + xOffset+ secondRowOffset,y+ yOffset, 0);
                    }

                    count++;
                }
            }
        }
    }

    void FetchCells()
    {
        cells = new List<HexCell>();
        HexCell[] hc = GetComponentsInChildren<HexCell>();
        cellsArray = new HexCell[height][];
        for (int i = 0; i < cellsArray.Length; i++)
        {
            cellsArray[i] = new HexCell[width - i % 2];
            
        }


        for (int j = 0; j < hc.Length; j++)
        {
            cellsArray[hc[j].y][hc[j].x]= hc[j];
            hc[j].SetActive(true);
            cells.Add(hc[j]);
            
            hc[j].name = "hexCell(" + hc[j].x + "," +hc[j]. y + ")";
            
        }

    }

    public void AdaptCamera()     
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
        float gridHeight = HexMetrics.longRadius * (height - 1) * 1.5f;

        cam.orthographicSize = gridHeight * 2.1f / 2f;


    }
 

  
    public void HighlightAvailableCells(bool t)
    {
        foreach(HexCell hc in cells)
        {
            hc.Highlight(hc.isAvailable() && t);
        }
    }

    public HexCell getCell(int x, int y)
    {
        return cellsArray[y][x];

    }

    public void Clear()
    {
        FetchCells();
        foreach(HexCell c in cells)
        {
            GamePiece gp=c.RemovePiece();
            if (gp != null)
            {
                Destroy(gp.gameObject);
            }
            Destroy(c.gameObject);
        }
        cells.Clear();
        cells = new List<HexCell>();
        
    }

    public void Build(int width, int height)
    {
        GenerateMesh(width, height, 1);
        FetchCells();
        AdaptCamera();
    }

}
