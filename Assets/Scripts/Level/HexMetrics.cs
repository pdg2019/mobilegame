﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HexMetrics
{
    public static float longRadius = 1f/2f;
    public static float shortRadius = 0.86602540378f/2f;

}
