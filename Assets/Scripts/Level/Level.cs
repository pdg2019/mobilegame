﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    private bool validationMode;

    public int localLevelIndex = -1;
    public  List<LaserReceiver> Receivers = new List<LaserReceiver>();
    private string json;

    public void setLocalLevelIndex(int i)
    {
        localLevelIndex = i;
    }
    public void setJson(string js)
    {
        json = js;
    }
    public string getJson()
    {
        return json;
    }
    public void AddReceiver(LaserReceiver laserReceiver)
    {
        Receivers.Add(laserReceiver);
    }
    public void Reset()
    {
        json = null;
        Receivers = new List<LaserReceiver>();
    }
    public void SetValidationMode(bool b)
    {
        validationMode = b;
    }
    public void UpdateStatus()
    {
        //Debug.Log("UpdatingStatus");
        bool finished = true;
        foreach(LaserReceiver lr in Receivers)
        {
            if (!lr.activated)
            {
                finished = false;
            }
        }
        if (finished&&GameMaster.Master.currentMode==Mode.play)
        {
            if (validationMode)
            {
                StartCoroutine(GameMaster.Master.endSreen.DisplayPublishingScreen());
            }
            else
            {
                if (localLevelIndex != -1)
                {
                    int currentLvlUnlocked = PlayerPrefs.GetInt("levelUnlocked");
                    Debug.Log("current unlocked Lvl=" + currentLvlUnlocked);
                    Debug.Log("lvlindexLvl=" + localLevelIndex);
                    if (localLevelIndex + 1 > currentLvlUnlocked)
                    {
                        Debug.Log("setting unlocked Lvl to" + localLevelIndex + 1);
                        PlayerPrefs.SetInt("levelUnlocked", localLevelIndex + 1);
                    }
                    StartCoroutine(GameMaster.Master.endSreen.DisplayLocalLevelEndScreen());
                }
                else
                {
                  
                    StartCoroutine(GameMaster.Master.endSreen.DisplayExploreLevelEndScreen());

                }
               
            }
            //Debug.Log("levelComplete");

        }


    }
   
    
}
