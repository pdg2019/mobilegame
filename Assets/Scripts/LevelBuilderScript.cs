﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelBuilderScript : MonoBehaviour
{
    public HexGrid grid;
    private GameMaster gameMaster;
    public  GameReader reader;

    private InventoryManager inventory;

    public int width=4;
    public int height=7;
    
    // Start is called before the first frame update
    void Start()
    {
        gameMaster = GameMaster.Master;
        
        inventory = gameMaster.inventory;
    }


    public void ResumeBuildingLevel()
    {
        
        StartCoroutine(ResumeBuildingLevelCoroutine(gameMaster.currentLevel.getJson()));
    }

    public IEnumerator ResumeBuildingLevelCoroutine(string json)
    {
        grid.Clear();
        yield return new WaitForEndOfFrame();

        
        gameMaster.SetMode(Mode.build);
        reader.Read(json);

        inventory.Show();
        List<int> IDs = reader.getInventory(json);
        
        inventory.InitializeBuilderMode();

        foreach(int id in IDs)
        {
            inventory.secondaryInventory.AddToInventory(id);
        }
        inventory.Display();

        
       
    }
    public void BeginBuildingNewLevel(int size)

    {
        if( size == 0)
        {
            width = 4;
            height = 7;
        }else if (size == 1)
        {
            width = 6;
            height = 11;
        }
        else
        {
            width = 8;
            height = 13;
        }
       // GetComponent<GooglePlayClient>().SignIn();
        StartCoroutine(BuildingNewLevel());


    }

    public IEnumerator BuildingNewLevel()

    {
        
        grid.Clear();
        yield return new WaitForEndOfFrame();
        
        grid.Build(width, height);
        gameMaster.SetMode(Mode.build);
        inventory.Show();
        inventory.InitializeBuilderMode();
   
      
    }


    public void BeginValidatingLevel(string json)
    {
        StartCoroutine(ValidatingLevel(json));
    }

    public IEnumerator ValidatingLevel(string json)
    {

        // string json = "{\"height\":7, \"width\":4, \"cells\" : [ [{\"empty\":false, \"pieceID\":1, \"orientation\":0}, {\"empty\":false, \"pieceID\":0, \"orientation\":60}, {\"empty\":true}, {\"empty\":true}],[{\"empty\":true}, {\"empty\":true}, {\"empty\":true}, {\"empty\":true}],[{\"empty\":true}, {\"empty\":true}, {\"empty\":true}, {\"empty\":true}],[{\"empty\":true}, {\"empty\":false, \"pieceID\":2, \"orientation\":0}, {\"empty\":true}, {\"empty\":true}],[{\"empty\":true}, {\"empty\":true}, {\"empty\":true}, {\"empty\":true}],[{\"empty\":true}, {\"empty\":true}, {\"empty\":true}, {\"empty\":true}],[{\"empty\":true}, {\"empty\":true}, {\"empty\":false, \"pieceID\":0, \"orientation\":0}, {\"empty\":true}]],\"inventory\": [0,0,3,0]}";

        grid.Clear();
        yield return new WaitForEndOfFrame();
        gameMaster.SetMode(Mode.play);
        GameMaster.Master.currentLevel.SetValidationMode(true);
        GameMaster.Master.currentLevel.setLocalLevelIndex(-1);

        reader.Read(json);

        inventory.Show();
        List<int> IDs = reader.getInventory(json);
        inventory.InitializeValidationMode(IDs);
    }

    public void BeginPlayingLevel(string json,int localLevelIdx)
    {
        StartCoroutine(PlayingLevel(json,localLevelIdx));
    }

    public IEnumerator PlayingLevel(string json, int localLevelIdx)
    {

     
        grid.Clear();
        yield return new WaitForEndOfFrame();
        gameMaster.SetMode(Mode.play);
        GameMaster.Master.currentLevel.SetValidationMode(false);
        GameMaster.Master.currentLevel.setLocalLevelIndex(localLevelIdx);

        reader.Read(json);
        
        inventory.Show();
        List<int> IDs = reader.getInventory(json);
        inventory.InitializePlayMode(IDs);
    }
}
