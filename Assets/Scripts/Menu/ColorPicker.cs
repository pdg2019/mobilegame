﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{
    public RadialMenu radialMenu;
    public Animator animator;


    public Image white;
    public Image red;
    public Image green;
    public Image blue;
    public Image yellow;
    public Image cyan;
    public Image magenta;

    private List<Image> imagesList;
    public List<Color> colors;


    
    // Start is called before the first frame update
    void Start()
    {
        imagesList = new List<Image>();
        imagesList.Add(white);
        imagesList.Add(red);
        imagesList.Add(green);
        imagesList.Add(blue);
        imagesList.Add(yellow);
        imagesList.Add(cyan);
        imagesList.Add(magenta);

    }
    private void Setup()
    {
        imagesList = new List<Image>();
        imagesList.Add(white);
        imagesList.Add(red);
        imagesList.Add(green);
        imagesList.Add(blue);
        imagesList.Add(yellow);
        imagesList.Add(cyan);
        imagesList.Add(magenta);
    }
    public void RefreshColor()
    {
        SelectColor(((Colorable)radialMenu.hostCell.piece).GetLaserColor());

    }

    public void Expand()
    {
        animator.SetBool("expanded", true);
    }
    public void Retract()
    {
        animator.SetBool("expanded", false);
    }
  
    private void SelectColor(LaserColor color)
    {
        if (imagesList == null)
        {
            Setup();
        }
        radialMenu.SelectColor(color);
        Retract();
        int i = 0;
        foreach (Image img in imagesList)
        {
            img.color = colors[i * 2 + 1];
            i++;
        }
        switch (color)
        {
            case LaserColor.white:
                imagesList[0].color = colors[0];
                break;
            case LaserColor.red:
                imagesList[1].color = colors[2];
                break;
            case LaserColor.green:
                imagesList[2].color = colors[4];
                break;
            case LaserColor.blue:
                imagesList[3].color = colors[6];
                break;
            case LaserColor.yellow:
                imagesList[4].color = colors[8];
                break;
            case LaserColor.cyan:
                imagesList[5].color = colors[10];
                break;
            case LaserColor.magenta:
                imagesList[6].color = colors[12];
                break;
        }

    }

    public void SelectWhite()
    {
        SelectColor(LaserColor.white);
    }
    public void SelectRed()
    {
        SelectColor(LaserColor.red);
    }
    public void SelectBlue()
    {
        SelectColor(LaserColor.blue);
    }
    public void SelectGreen()
    {
        SelectColor(LaserColor.green);
    }
    public void SelectMagenta()
    {
        SelectColor(LaserColor.magenta);
    }
    public void SelectCyan()
    {
        SelectColor(LaserColor.cyan);
    }
    public void SelectYellow()
    {
        SelectColor(LaserColor.yellow);
    }
}
