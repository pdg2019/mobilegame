﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSreenScript : MonoBehaviour
{
    public GameObject endScreenRoot;
    public GameObject publishingScreen;
    public GameObject endLevelScreen;

    public void Hide()
    {
        endLevelScreen.SetActive(false);
        publishingScreen.SetActive(false);
        endScreenRoot.SetActive(false);
    }
    public IEnumerator DisplayPublishingScreen()
    {

        endScreenRoot.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        endLevelScreen.SetActive(false);
        publishingScreen.SetActive(true);
    }
    public IEnumerator DisplayLocalLevelEndScreen()
    {
        endScreenRoot.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        endLevelScreen.SetActive(true);
        publishingScreen.SetActive(false);
        yield return new WaitForSeconds(0.9f);
        Hide();
        GameMaster.Master.menu.ExitLevel();
    }
    public IEnumerator DisplayExploreLevelEndScreen()
    {
        endScreenRoot.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        endLevelScreen.SetActive(true);
        publishingScreen.SetActive(false);
        yield return new WaitForSeconds(0.9f);
        Hide();
        GameMaster.Master.menu.ExitLevelToExplore();
    }
}
