﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelMenuPanel : MenuPanel
{
    public int levelUnlocked;
    public List<LevelSlot> levelSlots;
    public LevelPageScript levelPageScript;
    public Button prevButton;
    public Button nextButton;

    public IEnumerator FadeInLeft()
    {
        levelPageScript.SetVisible(true);

        if (!active)
        {
            levelPageScript.Refresh();
            active = true;

            for (int i = 0; i < levelSlots.Count - 1; i++)
            {

                StartCoroutine(levelSlots[i].FadeInLeft());
                yield return new WaitForSeconds(delayBetweenAnimStarts);
            }
            yield return StartCoroutine(levelSlots[levelSlots.Count - 1].FadeInLeft());
        }
    }
    public IEnumerator FadeInRight()
    {
        levelPageScript.SetVisible(true);

        if (!active)
        {
            levelPageScript.Refresh();
            active = true;

            for (int i = 0; i < levelSlots.Count - 1; i++)
            {

                StartCoroutine(levelSlots[i].FadeInRight());
                yield return new WaitForSeconds(delayBetweenAnimStarts);
            }
            yield return StartCoroutine(levelSlots[levelSlots.Count - 1].FadeInRight());
        }

    }
    public IEnumerator FadeOutRight()
    {
       

        if (active)
        {

            active = false;

            for (int i = 0; i < levelSlots.Count - 1; i++)
            {

                StartCoroutine(levelSlots[i].FadeOutRight());
                yield return new WaitForSeconds(delayBetweenAnimStarts);
            }
            yield return StartCoroutine(levelSlots[levelSlots.Count - 1].FadeOutRight());
        }
    }

    public IEnumerator FadeOutLeft()
    {
       
    
        if (active)
        {

            active = false;

            for (int i = 0; i < levelSlots.Count - 1; i++)
            {

                StartCoroutine(levelSlots[i].FadeOutLeft());
                yield return new WaitForSeconds(delayBetweenAnimStarts);
            }
            yield return StartCoroutine(levelSlots[levelSlots.Count - 1].FadeOutLeft());
        }
    }
    public override IEnumerator Display()
    {
        levelPageScript.SetVisible(true);
        levelPageScript.Refresh();
        yield return StartCoroutine(FadeInLeft());
       
    }
    public override IEnumerator Hide()
    {
        levelPageScript.SetVisible(false);
        levelPageScript.Refresh();
        yield return StartCoroutine(FadeOutLeft());
        
    }
}
