﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPageScript : MonoBehaviour
{
    
    public List<LevelSlot> levelSlots;

    public List<int> lvls;
    public List<string> lvlsJsons;
    private int _currentPage;
    public Button nextButton;
    public Button prevButton;
    public LevelMenuPanel menuPanel;
    private bool visible=false;

    
    public int CurrentPage
    {
        get => _currentPage;
        set
        {
            int clampedValue = Mathf.Clamp(value, 0, lvls.Count / 24);
            _currentPage = clampedValue;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentPage = 0;
        DisplayPage(CurrentPage);
    }



    private void DisplayPage(int p)
    {

        int levelUnlocked = PlayerPrefs.GetInt("levelUnlocked");
        if (p > lvls.Count/24)
        {
            throw new Exception("NON-VALID Page");
        }

        for(int i = 0; i < 24; i++)
        {
            if(i + p * 24< lvlsJsons.Count)
            {
                int lvl = 1 + i + p * 24;

                
                levelSlots[i].Set(1 + i + p * 24, lvl <= levelUnlocked);
                
                
            }
            else
            {
                levelSlots[i].Hide();
            }
        }

        DisplayNavigationArrows();
    }

    private void DisplayNavigationArrows()
    {
        if (_currentPage > 0 && visible)
        {
            prevButton.gameObject.SetActive(true);
        }
        else
        {
            prevButton.gameObject.SetActive(false);
        }

        if(_currentPage< lvls.Count / 25 && visible)
        {
            nextButton.gameObject.SetActive(true);
        }
        else
        {
            nextButton.gameObject.SetActive(false);
        }
    }
    public void Refresh()
    {
        DisplayPage(CurrentPage);
    }
    public void Reset()
    {
        CurrentPage = 0;
        DisplayPage(CurrentPage);
    }
    public IEnumerator SwipeLeft()
    {
        
        yield return menuPanel.StartCoroutine(menuPanel.FadeOutLeft());
            
        DisplayPage(CurrentPage);
        menuPanel.StartCoroutine(menuPanel.FadeInRight());
    }
    public IEnumerator SwipeRight()
    {

        yield return menuPanel.StartCoroutine(menuPanel.FadeOutRight());

        DisplayPage(CurrentPage);
        menuPanel.StartCoroutine(menuPanel.FadeInLeft());
    }
    public void Next()
    {
        CurrentPage++;
        StartCoroutine(SwipeLeft());
        
    }

    public void Prev()
    {
        CurrentPage--;
        StartCoroutine(SwipeRight());
    }
    public void SetVisible(bool b)
    {
        visible = b;
    }
}
