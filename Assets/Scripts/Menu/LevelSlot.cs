﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(RectTransform))]
public class LevelSlot : MonoBehaviour
{
    private Text _text;
    private Image _image;
    private Button _button;




    private int levelIndex;

    
    private float  originalLeft;
    private float  originalRight;

    private float startLeft;
    private float startRight;

    private float currentLeft;
    private float currentRight;

    private float targetLeft;
    private float targetRight;
    
 
    private RectTransform rectTransform;

    private float startFadeTime;
    public float fadeDuration;
 
    private float targetAlpha;
    public Text Text {
        get { 
                if (_text == null)
                {
                    _text = GetComponentInChildren<Text>();
                }
                return _text;
        }
    }

    public Image Image {
        get{
            if (_image == null)
            {
                _image = GetComponent<Image>();
            }
            return _image;
        }
    }

    public Button Button
    {
        get
        {
            if (_button == null)
            {
                _button = GetComponent<Button>();
            }
            return _button;
        }
    }

    public void Start()
    {
        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
        originalLeft = rectTransform.offsetMin.x;
        originalRight = rectTransform.offsetMax.x;
        currentLeft = originalLeft;
        currentRight = originalLeft;
        targetLeft = originalLeft;
        targetRight = originalRight;
    }
    

    
    public void Set(int level,bool unlocked)
    {
        levelIndex = level-1;
        Text.enabled = true;
        Text.text = level.ToString();
        Image.enabled = true;
        Button.enabled = unlocked;
        if (unlocked)
        {
            Image.color = GameMaster.Master.unlockedLevelColor;
        }
        else
        {
            Image.color = GameMaster.Master.lockedLevelColor;
        }
    }


    public void Hide()
    {
        Image.enabled = false;
        Button.enabled = false;
        Text.enabled = false;
    }

    public IEnumerator FadeInLeft()
    {
        startFadeTime = Time.time;

        startLeft = originalLeft - Screen.width;
        startRight = originalRight + Screen.width;
    

        targetLeft = originalLeft;
        targetRight = originalRight;
        targetAlpha = 1;

        yield return StartCoroutine(fade());

    }
    public IEnumerator FadeInRight()
    {
        startFadeTime = Time.time;

        startLeft = originalLeft + Screen.width;
        startRight = originalRight - Screen.width;


        targetLeft = originalLeft;
        targetRight = originalRight;
        targetAlpha = 1;
        yield return StartCoroutine(fade());

    }
    public IEnumerator FadeOutRight()
    {
        startFadeTime = Time.time;

        startLeft = originalLeft;
        startRight = originalRight;


        targetLeft = originalLeft + Screen.width; 
        targetRight = originalRight - Screen.width;
        targetAlpha = 0;

        yield return StartCoroutine(fade());

    }

    public IEnumerator FadeOutLeft()
    {
       // Debug.Log("starting");
        startFadeTime = Time.time;

        startLeft = originalLeft;
        startRight = originalRight;


        targetLeft = originalLeft - Screen.width;
        targetRight = originalRight + Screen.width;
        targetAlpha = 0;

        yield return StartCoroutine(fade());

    }

    private void SetTransparency(float t)
    {
        t = Mathf.Clamp01(t);
        Image.color = new Color(Image.color.r, Image.color.g, Image.color.b, t);
        Text.color = new Color(Text.color.r, Text.color.g, Text.color.b, t);
    }
    private IEnumerator fade()
    {
        while (Time.time <= startFadeTime + fadeDuration)
        {
            float t = (Time.time - startFadeTime) / fadeDuration;
            currentLeft = Mathf.Lerp(startLeft, targetLeft, t);
            currentRight = Mathf.Lerp(startRight, targetRight, t);
            RectTransformExtensions.SetLeft(rectTransform, currentLeft);
            RectTransformExtensions.SetRight(rectTransform, currentRight);
            float a= Mathf.Lerp(1-targetAlpha, targetAlpha, t);
            SetTransparency(t);
            yield return new WaitForEndOfFrame();              
        }
        RectTransformExtensions.SetLeft(rectTransform, targetLeft);
        RectTransformExtensions.SetRight(rectTransform, targetRight);
        SetTransparency(targetAlpha);
    }


    public void StartLevel()
    {
        LevelPageScript lps= transform.parent.gameObject.GetComponent<LevelPageScript>();
        if(levelIndex< lps.lvlsJsons.Count)
        {
            GameMaster.Master.menu.PlayLevel(lps.lvlsJsons[levelIndex],levelIndex+1); 
        }
    }
  

}
