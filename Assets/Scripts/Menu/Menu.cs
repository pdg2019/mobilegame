﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Menu : MonoBehaviour
{
    private List<MenuPanel> panels;
    public MenuPanel openingPanel;
    public MenuPanel playPanel;
    public MenuPanel explorePanel;
    public MenuPanel levelPanel;
    public MenuPanel SizeSelectPanel;
    public LevelPageScript levelPageScript;
    public Button backButton;
    private GameMaster gameMaster;

    // Start is called before the first frame update
    void Start()
    {
        gameMaster = GameMaster.Master;
        panels = new List<MenuPanel>();
        panels.Add(openingPanel);
        panels.Add(playPanel);
        panels.Add(explorePanel);
        panels.Add(levelPanel);
        panels.Add(SizeSelectPanel);
        backButton.gameObject.SetActive(false);
        Debug.Log("display");
        StartCoroutine("Display", 0);
    }

    public IEnumerator Display(int index)
    {
        Debug.Log("displaying");
        if (index >= panels.Count)
        {
            throw new Exception("index out of bounds");
        }
        else
        {
            
            for(int i = 0;i< panels.Count; i++) {
                if (i != index)
                {

                    yield return StartCoroutine(panels[i].Hide());
                }
                
            }
            if (index >= 0)
            {
                yield return StartCoroutine(panels[index].Display());
            }
        }
    }
    public void HideEverything()
    {

        StartCoroutine("Display", -1);
    }
    public void DisplayOpeningPanel()
    {
        StartCoroutine("Display", 0);
        backButton.gameObject.SetActive(false);
    }

    public void DisplayPlayPanel()
    {

        backButton.gameObject.SetActive(true);

        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayOpeningPanel);
        StartCoroutine("Display", 1);
        
       
    }
    
    public void DisplayExplorePanel()
    {

        backButton.gameObject.SetActive(true);

        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayPlayPanel);
        StartCoroutine("Display", 2);
    }


    public void DisplaySizePanel()
    {
        backButton.gameObject.SetActive(true);
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayOpeningPanel);
        StartCoroutine("Display", 4);
    }
    public void DisplayLevelPanel()
    {
        backButton.gameObject.SetActive(true);
        levelPageScript.Reset();
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayPlayPanel);
        StartCoroutine("Display", 3);
      
    }
    public void ExitLevel()
    {
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayPlayPanel);
        gameMaster.ExitPlay(3);
    }
    public void ExitLevelToExplore()
    {
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayPlayPanel);
        gameMaster.ExitPlay(2);
    }
    public void ExitBuild()
    {
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(DisplayOpeningPanel);
        gameMaster.ExitPlay(4);
    }
    public IEnumerator OpenBuildScene(int size)
    {
        backButton.gameObject.SetActive(true);
        backButton.onClick.RemoveAllListeners();
        yield return Display(-1);
        gameMaster.levelBuilder.BeginBuildingNewLevel(size);
        backButton.onClick.AddListener(ExitBuild);

    }
    public void BuildLevel(int size)
    {
        StartCoroutine( OpenBuildScene(size));
    }

    public IEnumerator OpenPlayScene(string json, int localLevelIdx)
    {
        Debug.Log("Starting Lvl");
        backButton.onClick.RemoveAllListeners();
        yield return Display(-1);
        
        backButton.onClick.AddListener(ExitLevel);
        gameMaster.levelBuilder.BeginPlayingLevel(json,localLevelIdx);

    }
    public void PlayLevel(string json)
    {
        StartCoroutine(OpenPlayScene(json,-1));
    }

    public void PlayLevel(string json, int localLevelIdx)
    {
        StartCoroutine(OpenPlayScene(json,localLevelIdx));
    }
}
