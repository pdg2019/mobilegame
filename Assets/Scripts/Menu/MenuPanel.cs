﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour
{
    public List<Animator> menuItems;
    public float delayBetweenAnimStarts;
    public bool active=false;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public virtual IEnumerator Display()
    {
        if (!active)
        {
            active = true;
            Animator lastAnimator = menuItems[0];
            foreach (Animator a in menuItems)
            {
                lastAnimator = a;
                a.SetTrigger("FadeIn");
                yield return new WaitForSeconds(delayBetweenAnimStarts);
            }
            yield return new WaitForSeconds(lastAnimator.GetCurrentAnimatorStateInfo(0).length + lastAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        }
    }
    public virtual IEnumerator Hide()
    {
        if (active)
        {
            active = false;
            Animator lastAnimator =menuItems[0];
            int i = 0;
            foreach (Animator a in menuItems)
            {
                
                lastAnimator = a;
                a.SetTrigger("FadeOut");
                if(i< menuItems.Count-1)
                {
                    yield return new WaitForSeconds(delayBetweenAnimStarts);
                }
                i++;
            }
            yield return new WaitForSeconds(lastAnimator.GetCurrentAnimatorStateInfo(0).length);
        }
    }
    // Update is called once per frame
   
}
