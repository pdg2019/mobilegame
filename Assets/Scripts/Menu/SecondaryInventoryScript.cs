﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;
public class SecondaryInventoryScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Animator animator;
    public float timeToRetract;
    private bool cursorOn;
    private float cursorExitTime;




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!cursorOn&& Time.time > cursorExitTime + timeToRetract)
        {
            animator.SetBool("Deployed", false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        cursorOn = true;
        animator.SetBool("Deployed", true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        cursorOn = false;
        cursorExitTime = Time.time;
    }

    public void Hide()
    {
        animator.SetBool("Hidden", true);
    }
    public void Show()
    {
        animator.SetBool("Hidden", false);
    }
}
