﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Utility : MonoBehaviour
{
   public  GraphicRaycaster m_Raycaster;
    public Canvas UI;
    public EventSystem m_EventSystem;
    // Start is called before the first frame update
  


    public static int AngleFromDir(Vector2 dir)
    {
        return Mathf.RoundToInt(Mathf.Abs(Vector2.Angle(dir, Vector2.right)))*(int)Mathf.Sign(dir.y);
    }
    public bool IsOverSecondaryInventory(Vector2 pos)
    {
        //Debug.Log("checking if"+pos+" over secondary");
        //Set up the new Pointer Event
        PointerEventData m_PointerEventData = new PointerEventData(m_EventSystem);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;
        

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        m_Raycaster.Raycast(m_PointerEventData, results);

       
        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
           // Debug.Log("who is " + result.gameObject);
            if (result.gameObject.tag == "SecondaryInventory")
            {
             //   Debug.Log("YES IT IS");
                return true;
            }
        }

       // Debug.Log("nope");
        return false;
    }
    public HexCell FindClosestFreeCell(Vector2 pos)
     {

      
         Collider2D[] potentialCells = Physics2D.OverlapCircleAll(pos, .3f, GameMaster.Master.cellsLayerMask, -10f, 10f);
    HexCell closestCell;

    float closestCellDistance = 10000;
        //if too far from any cell
       
        if (potentialCells.Length == 0)
        {
            return null;
        }
        else
        {
            closestCell =null;
            closestCellDistance = 1000;
            for(int i =0;i<potentialCells.Length;i++)
            {
          
                HexCell hc = potentialCells[i].GetComponent<HexCell>();

                if (hc!=null && hc.isAvailable())
                {
                    float testDist = Vector2.Distance(potentialCells[i].transform.position, pos);

                    
                    if (testDist<closestCellDistance)
                    {
                        
                        closestCellDistance = testDist;
                        closestCell = hc;
                    }
                }
              
            }

            if (closestCell != null)
            {
                return closestCell;
            }
            else
            {
                return null;
            }
           
        }
        

    }


}
