﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class HexGridTest
    {
        
      

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator HexgridBuildsCorrectly()
        {
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/GameMaster"));
            
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Camera"));
            GameObject gridGameObject  =MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Grid"));
            yield return new WaitForEndOfFrame();
            GameMaster gameMaster = GameMaster.Master;
            gameMaster.currentMode = Mode.play;
            HexGrid grid = gridGameObject.GetComponent<HexGrid>();
            // 3
            //yield return new WaitForEndOfFrame();
            //yield return new WaitForSeconds(.5f);

            grid.Build(2, 4);
           // yield return new WaitForSeconds(.5f);

            Assert.AreEqual(6, grid.cells.Count);
            
           
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
