# CI/CD for the project

## CI/CD pour projets  Unity

Les projets développés sous Unity, notamment pour le jeu vidéo, contiennent fréquemment des fichiers de grandes tailles comme des binaires ou des graphismes nécessaires au build de l'application, ce qui entraîne une taille de projet souvent conséquente. De ce fait l'utilisation de pipeline pour du développement continu sur Unity devient rapidement compliqué car un simple build peut parfois prendre plusieurs heures.

Un autre facteur pouvant ralentir le pipeline est l’exécution des tests unitaires. En effet, il existe deux types de tests unitaires, les tests « Edit Mode » qui se concentrent sur le code et sont exécutés dans l'éditeur et les tests « PlayMode » qui s'exécutent dans une fenêtre extérieure et vont interagir avec les différentes scènes de l'application ainsi que les différentes routines pour tester le comportement du jeu. Les tests PlayMode sont donc plus intéressant lorsque l'application commence à prendre de l'ampleur, cependant ceux-ci doivent charger une plus grande partie du projet avant de se lancer ce qui implique qu'un pipeline peut se retrouver à exécuter des tests automatiques et des builds sur des périodes de temps très longues.

De plus les outils d'intégration classiques tels que GitLab CI ou Travis ne possèdent pas de compilateur Unity intégré, ce qui implique de devoir passer par un docker contenant l'éditeur Unity ou de lancer les tests sur sa machine en ligne de commande depuis l'outil utilisé.

Nous avons constaté qu’Unity offre un service d'intégration continue avec [Unity Cloud Build](https://docs.unity3d.com/560/Documentation/Manual/UnityCloudBuild.html), malheureusement ce service n'est disponible que pour les projets ayant souscris à Unity Team avancé inclus dans Unity Pro (et ne contenant que 3 collaborateurs dans la version de base).

Une autre solution potentielle semble être [Perforce](https://www.perforce.com/), cependant s’agissant aussi d'une alternative payante, nous n'avons pas fait de recherches approfondies à ce sujet.

## Notre Implémentation
Pour notre projet la contrainte d'utilisation de GitLab et donc de GitLab CI pour l'intégration continue nous laissait deux choix d'implémentation :
* Utilisation d'un runner local utilisant l'éditeur Unity installé sur la machine du développeur et utilisation de scripts de tests (https://dzone.com/articles/the-road-to-continuous-integration-in-unity)

* Utilisation d'une image Docker contenant l'éditeur Unity et lancement des tests dans un container.

La première solution étant difficile à mettre en place, du fait que chaque personne doit mettre en place le Runner sur son ordinateur et que l'installation est compliquée sur Ubuntu (de par le fait que Unity est arrivé récemment sur cet OS).

Nous avons donc opté pour la solution de l'image docker en utilisant une [image unity](https://gitlab.com/gableroux/unity3d-gitlab-ci-example) développé par @gableroux. Les tests et les builds sont donc effectués dans un container docker contenant un éditeur Unity. Nous avons également mis en place plusieurs runners sur une machine virtuelle fournie par la HEIG pour que tout le groupe puisse lancer les builds automatiques depuis leur ordinateur sans avoir de procédure ou d'installation particulière à effectuer.

## Problèmes rencontrés
Nous utilisons Firebase pour le stockage des niveaux et des utilisateurs. Celui-ci s'intègre facilement à Unity grâce à un [plug-in](https://firebase.google.com/docs/unity/setup), malheureusement cela provoque des scripts failure dans l'exécution de Unity dans le container. En effet, il faut réinstaller le plug-in dans le container à chaque fois lors du lancement de Unity en ligne de commandes et malgré nos efforts nous n'avons pas réussi à corriger ces bugs. Après des tests sur Unity sous Ubuntu nous avons rencontrés des problèmes similaires et comme expliqué dans la section ci-dessus, Unity n'est pas supporté nativement par GitLab et après des recherches sur différents forums, il apparaît que le lancement en ligne de commande est également une des faiblesses de l'éditeur. Notre niveau en développement d'images docker étant limité nous ne pouvons donc pas modifier celle-ci pour la faire convenir à nos besoins, de plus l'image étant un projet indépendant de Unity nous ne savons pas quand @gableroux rajoutera une compatibilité avec Firebase.

De plus notre application etant déployé sur le play store de Android avec un build automatique de notre jeu sur unity aurait permis un deploiement automatique également cependant l'absence d'alternative gratuite a Unity Cloud Build nous empeche de mettre en place une pipeline efficace malgrés nos differents essais.

## Conclusion
Unity possède un système d'unit testing intégré ce qui permet de faire facilement des tests unitaires en prenant en compte les différents paramètres de l'application. Cependant Unity n'est pas vraiment adapté pour être utilisé avec GitLab CI car celui-ci ne possède pas le compilateur Unity. Des alternatives sont possibles mais celles-ci restent très limitées et après pas mal de recherches sur différents forums il ressort que Unity est difficile à adapter aux méthodologies standards de CI/CD du fait de la taille des projets et de l'utilisation d'un logiciel propriétaire nécessitant une licence.
