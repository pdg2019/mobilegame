# Installer notre projet

## Application Android
Une béta ouverte de notre application est disponible sur la landing page ou à cette [adresse](https://play.google.com/apps/testing/com.pdg2019.MirrorHex).

## Projet Unity
Notre projet a été développé avec la version 2019.2.5f1 de Unity, il est donc conseillé d'utiliser cette version pour ouvrir notre projet. Si vous possédez déjà une version d'Unity, il est facile d'en installer une seconde grâce à Unity Hub.

Cloner ensuite notre repository sur votre ordinateur. Les fichiers liés à Firebase n'étant pas sur notre repo, il est nécessaire de l'installer localement. [Télécharger](https://firebase.google.com/download/unity). Dézipper le dossier, uniquement le dossier `dotnet4` est nécessaire.
Ouvrez notre repo avec Unity, allez ensuite dans `Assets` -> `Import Package` -> `Custom Package` puis ajouter `FirebaseAuth.unitypackage` et `FirebaseDatabase.unitypackage` se trouvant dans le dossier `dotnet4`. 
Téléchargez le plugin "Play games" à [cette adresse](https://github.com/playgameservices/play-games-plugin-for-unity/tree/master/current-build) et ajoutez le au projet comme fait précédemment avec les packages Firebase.
Dans `File` -> `Build Settings`, changez de plateforme pour passer à Android et ouvrez la `MainGameScene`.
Vous pouvez à présent lancer notre jeu avec le bouton "Play".