# Planning

## Semainne 16/12 - 22/12

### Front-end:
* Implémenter la sauvegarde des niveaux
* Implémenter la logique de fin de niveau(passage au niveau suivant, retour au menu principal)
* implémenter les prismes

### Back-end:

* Mise en place de la sécurité sur la database Firebase
* Lecture et ouverture des niveaux stockés dans la database

### DevOps

* Investiguer les scripts failure dans le build automatique du pipeline (problèmes d'ajout de plugin unity dans le docker)

## Semaine 23/12-29/12 AkA semaine de Noel

### Front-end:
* Fabrication de niveaux pour le "mode campagne"(24 niveaux)

### Back-end:
 * Exploration des niveaux dans la database

### DevOps:
* Réparer le pipeline ou voir pour une alternative compatible avec unity

## Semaine 30/12-05/01

### Front-end:

* Refactoring
* Correction des derniers bugs
* Lors de la construction du niveau pouvoir selectionner la taille du Grid


### Back-end:

* Système de progression du joueur (stocker quels niveaux sont déjà complétés, où en est le joueur dans le niveau)
* Vote sur les niveaux en ligne

### DevOps:
* Compléter la documentation
* Clean du Repository avant le code freeze
* compléter les tests
