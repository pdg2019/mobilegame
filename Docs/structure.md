# Structure de notre repo
Cette courte documentation a pour but de décrire la structure de notre repository.
## Unity
- Le dossier `Assets` de notre repo contient des assets Unity. Cela peut être des scripts, les scènes, sprites, prefabs, ...
- Le dossier `ProjectSettings` contient diverses configurations liées au projet Unity
- Le dossier `ci` contient les scripts et configurations liés au conatainer Unity de @gableroux tournant sur la VM de la HEIG

## Autre
- Le dossier `Docs` contient nos divers fichiers de documentation
- Le dossier `LandingPage` contient tout ce qui concerne la landing page (ressources, `index.html`)
- Le dossier `Mockups` contient les maquettes de notre jeu
