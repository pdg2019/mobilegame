# Pdg2019-mobileGame

## Project description:
Our project is a small casual android puzzle game where the player has to guide lasers to their goal using multiple tools such as mirrors, prisms, portals etc...

We intend to implement level building and sharing as well to give the opportunity to players to explore and solve a (potentionally) massive amount of levels.

You can find the landing page [here](https://pdg2019.gitlab.io/mobilegame/)

## MVP (Minimal Viable Product) definition:

### General MVP:
For our project, the Minimal Viable Product (MVP) consist mainly on our game to be functional (and fun!).
To clarify, we want a minimum of 10 levels, but we will probably add more. A level editor is also recquired. The user must be able, at the very least, to create, edit and save levels. It will also be possible to share levels between users.

### About the game:
The basic functionality of the game are the following:
* Users can win a level.
* Beating a level result in the unlocking of the next one.
* Users can see the current state of the game on a hex grid
* Users can place, rotate and move mirrors that reflect lasers.
* Users can place, rotate and move prisms that combine or decompose the colors of the lasers.

### About the editor:
The basic functionality of the editor are the following:
* Users can place any component of the game in whatever hex.
* Users can save the current state of the edit.
* Users can share the finished level with their pairs.


## Demo scenario
The final demo will feature the following points:
* A demo of the tutorial/first levels.
	* Presentation of the UI elements.
	* Presentation of the basic mechanics.
	* Presentation of the different complex elements.
	* Presenting the progression system.
	* Presentation of the saved progress.
* A demo of the level editor (simple level creation).
	* Placing elements.
	* Rotating elements.
	* Moving elements.
	* Removing elements.
	* Saving the level.
* A demo of the level sharing.
* A short introduction to unity.
* A talk from every person in the group about their respective domain.

## Long term vision
In this paragraph, we will talk about what would be the ideal goal if we had infinite amount of time and money to make the perfect final product.

The global idea would be to have a marketable product. There would be a marketing goal, to advertise our product. The idea to make the game worth the time invested would be to integrate advertisement to our game. There could be, for example, advertisement between levels or an in-game currency purchasable with real-life money used to buy cosmetic items. To promote the level sharing feature, we could award the player with a handful of in-game currency for each player playing their levels.

Iterating on the level sharing feature, there could be a voting system used to create a "community environment" where people can browse the most popular levels and play them, or discover new or trending levels created by the community.

Gameplay-wise, the game would eventually stay more or less like we plan to make it. We will surely add mechanics that are not yet defined precisely, but that will not change the flow of the game.

Porting the game to iOS would also be a priority, knowing that for now nobody in our team has the correct setup to develop on iOS. There are multiple people using iOS devices that could be interested in our project.


## Documentation

Ci-dessous se trouvent les liens vers notre documentation.

- [Installer notre projet](https://gitlab.com/pdg2019/mobilegame/blob/MasterCorrected/Docs/install.md)
- [Structure du repo](https://gitlab.com/pdg2019/mobilegame/blob/MasterCorrected/Docs/structure.md)

### CI/CD

[Slides de la presentation 2](https://gitlab.com/pdg2019/mobilegame/blob/MasterCorrected/Docs/slides.pdf)

[Documentation on CI/CD](https://gitlab.com/pdg2019/mobilegame/blob/MasterCorrected/Docs/ContinuousIntegration.md)
